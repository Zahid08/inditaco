<?php
session_start();
include "includes/header.php";
?>
<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
    <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
        <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font mb-4 mt-3 text-6xl text-white">Our Story</h1>
            <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Indi Taco is a ghost kitchen that is accessible through online ordering</p>
        </div>
    </div>
</section>
<div class="bg-texture">
    <section class="text-gray-600 body-font">
        <div class="container flex flex-wrap px-5 py-24 mx-auto items-center">
            <div class="md:w-1/2 md:pr-12 md:py-8 md:border-r md:border-b-0 mb-10 md:mb-0 pb-10 border-b border-black">
                <div class="lg:flex-grow mt-10 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
                    <h1 class="title-font mb-4 mt-3 text-4xl text-white">Classic Tacos<br class="hidden lg:inline-block"> Reinvented</h1>                   
                </div>
            </div>
            <div class="flex flex-col md:w-1/2 md:pl-12">
                <p class="mb-6 leading-relaxed subtitle-font text-xl text-white">Born and raised as an Indian boy, with a ferocious drive and ambition within the Tex-Mex food industry, led Ankit Patel on a journey to bring his dream into reality. In 2010 Ankit created his family business Sandwich Time Deli & Tex-Mex. The menu consists of enormous burritos, towering sandwiches, with sides and homemade signature sauces that led customers flying right in. He not only is the brains behind the menu, but knows how to cook each item, and markets his own business. Ankit and Sandwich Time Deli have been recognized by NJ.com, Thrillest, VeganNJ, and other well known social platforms. Sandwich Time is known for its popular tasty food at an economical price.</p>
                <p class="mb-6 leading-relaxed subtitle-font text-xl text-white">After mastering every foodies Tex-Mex dream, Ankit decided to start a modern ghost kitchen that combines his Indian heritage and Mexican food experience to pay homage to both cuisines. A lot of time and energy was spent on perfecting the menu, flavors, photography, and all other miniature details to ensure Ankit's ghost kitchen would provide the same delicious quality style food as Sandwich Time Deli. Ankit, his wife, and newborn baby are a young family with a young concept looking for your support to help this local business succeed! They have worked diligently on creating special sauces and gravies, indian seasoned meats/fillings, and multiple vegan options. These items were tested within his current Sandwich Time Deli community and altered according to customer feedback. Many authentic spices were recommended by local customers and incorporated into his new menu. Indi-Taco is born. </p>
            </div>
        </div>
    </section>
    <section class="body-font" style="background-image: url(assets/images/banner-about.jpg); height:550px">
        <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
            <div class="lg:flex-grow md:w-1/2 mt-20 flex flex-col md:items-center md:text-center mb-16 md:mb-0 items-center text-center">
                <h1 class="title-font mb-4 mt-3 text-6xl text-white">Simplicity in our spices with a modern twist to our ingredients</h1>
                <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Indi Taco is a ghost kitchen that is accessible through online ordering on this website</p>
                <div class="flex justify-center">
                    <a href="shop.php" class="main-button" type="submit">Order Now</a>
                </div>
            </div>
        </div>
    </section>
    <?php
    include "includes/subscribe.php";
    ?>
</div>

<?php
include "includes/footer.php";
?>