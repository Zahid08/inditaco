
<?php
require('app/connection.inc.php');
session_start();

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

?>


<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/style.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body class="antialiased">
    <header class=" body-font p-2 bg-black">
        <div class="container mx-auto flex flex-wrap p-6 flex-col md:flex-row items-center">
            <a href="index.php" class="flex items-center mb-4 md:mb-0">
                <div class="logo"><img src="assets/images/inditaco-logo.png"></div>
                <span class="ml-3 logo-text text-xl">indi <span>taco</span></span>
            </a>
            <nav class="md:ml-auto flex flex-wrap items-center text-base text-white justify-center">
                <a href="our-story.php" class="mr-8 ">Our Story</a>
                <a href="catering.php"class="mr-8 ">Catering</a>
                <a href="update.php"class="mr-8 ">Updates</a>
                <a href="contact.php" class="mr-8 ">Contact us</a>
                <a href="shop.php" class="mr-8 ">ORDER NOW</a>
                <a href="auth.php"class="mr-8 ">
                    <?php if (isset($_SESSION['name'])) {
                    echo $_SESSION['name'];
                    } else { echo"Login / Register";} ?>
                </a>
                <a href="logout.php"class="mr-8 ">
                    <?php if (isset($_SESSION['name'])) {
                    echo "Logout";
                    } else {} ?>
                </a>
                <a href="cart_page.php"  >
                <svg version="1.1" id="Capa_1"  class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="30px" height="30px" viewBox="0 0 902.86 902.86" style="enable-background:new 0 0 902.86 902.86;"
	 xml:space="preserve">
<g>
	<g>
		<path d="M671.504,577.829l110.485-432.609H902.86v-68H729.174L703.128,179.2L0,178.697l74.753,399.129h596.751V577.829z
			 M685.766,247.188l-67.077,262.64H131.199L81.928,246.756L685.766,247.188z"/>
		<path d="M578.418,825.641c59.961,0,108.743-48.783,108.743-108.744s-48.782-108.742-108.743-108.742H168.717
			c-59.961,0-108.744,48.781-108.744,108.742s48.782,108.744,108.744,108.744c59.962,0,108.743-48.783,108.743-108.744
			c0-14.4-2.821-28.152-7.927-40.742h208.069c-5.107,12.59-7.928,26.342-7.928,40.742
			C469.675,776.858,518.457,825.641,578.418,825.641z M209.46,716.897c0,22.467-18.277,40.744-40.743,40.744
			c-22.466,0-40.744-18.277-40.744-40.744c0-22.465,18.277-40.742,40.744-40.742C191.183,676.155,209.46,694.432,209.46,716.897z
			 M619.162,716.897c0,22.467-18.277,40.744-40.743,40.744s-40.743-18.277-40.743-40.744c0-22.465,18.277-40.742,40.743-40.742
			S619.162,694.432,619.162,716.897z"/>
	</g>
</g>
</svg>

</a>

            </nav>           
        </div>
    </header>