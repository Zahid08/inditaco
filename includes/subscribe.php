<section class=" body-font">
            <div class="container px-8 py-16 mx-auto">
                <div class="flex flex-wrap -m-4">
                    <div class="p-4 lg:w-3/12">

                    </div>
                    <div class="p-4 lg:w-9/12">
                        <section class=" body-font overflow-hidden">
                            <div class="container px-8 pb-16 mx-auto">
                                <p class="mb-6 leading-relaxed subtitle-font text-xl sc-color">Subscribe</p>
                                <h3 class="title-font mb-4 mt-3 text-4xl text-white">Find Latest Exclusive<br> Offer and Update</h3>
                                <form class="subscribe">
                                    <input type="text" placeholder="Enter your Email">
                                    <a class="sub-button" class="text-center" href="index.php?success=Thanks for Subscribing with us" type="submit">Subscribe</a>
                                    <div class="mr-4 subtitle-font text-white text-xl pt-4">
                                    <?php
                                    if (isset($_GET['success'])) {
                                        echo $_GET['success'];
                                      }
                                    ?>
                                    </php>
                                </form>
                                <div class="flex flex-wrap mt-12 leading-relaxed subtitle-font text-xl text-white">
                                    Design with &nbsp; &nbsp; &nbsp;<svg height="24" version="1.1" width="24" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                                        <g transform="translate(0 -1028.4)">
                                            <path d="m7 1031.4c-1.5355 0-3.0784 0.5-4.25 1.7-2.3431 2.4-2.2788 6.1 0 8.5l9.25 9.8 9.25-9.8c2.279-2.4 2.343-6.1 0-8.5-2.343-2.3-6.157-2.3-8.5 0l-0.75 0.8-0.75-0.8c-1.172-1.2-2.7145-1.7-4.25-1.7z" fill="#c0392b" />
                                        </g>
                                    </svg> &nbsp; &nbsp; &nbsp; <span> Digital Future Media &nbsp; &nbsp; &nbsp; || </span>

                                    <span class="ml-8">Power by Salesberry</span>
                                </div>

                            </div>
                        </section>
                    </div>

                </div>
            </div>
        </section>