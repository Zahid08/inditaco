<?php
include "includes/header.php";
require('../inditaco/app/cart_details.php');
require('../inditaco/app/connection.inc.php');
$cartItemsList = getCartItems($conn, $bid);

?>
<style>
    span.btnqty.qtyplus.icon.icon-plus.disbaleItem {
        pointer-events: none;
    }
</style>
<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
    <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
        <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font mb-4 mt-3 text-6xl text-white">Cart</h1>
            <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Lorem Ipsum some tagline about us or our story</p>
        </div>
    </div>
</section>
    <form  method="post" action="<?=$baseUrl?>/app/update_cart.php">
        <div style="background-color: #0c0c0c;">
            <section class="text-gray-600 body-font relative">
                <div class="container px-5 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
                        <div class="lg:w-2/3 md:w-1/2 ">
                            <div class="p-4">
                                <div class="border-2 rounded-lg border-black border-opacity-50 p-8">
                                    <?php
                                    $suntotal = 0;
                                    if ($cartItemsList) {
                                        foreach ($cartItemsList as $keyItemIndex => $items) {
                                            $maxQuantity=$items['maxQuantity'];
                                            $suntotal += $items['totalSalesPrice'];
                                            $getSingleProduct = getSingleProduct($items['productid'], $items['variationsId'], $bid, $conn);
                                            if (isset($_SESSION['user'])) {
                                                $cartId = $keyItemIndex;
                                                $indexItem=$keyItemIndex;
                                            } else {
                                                $cartId = $items['cart_id'];
                                                $indexItem=$keyItemIndex;
                                            }
                                    ?>
                                            <div class="p-4 py-6 border-b-2 border-black">
                                                <div class="flex sm:flex-row flex-col">
                                                    <div class="flex-grow inline-flex overflow-hidden items-center">
                                                        <h2 class="sm:text-base text-base title-font text-white"><?= $getSingleProduct['product_name'] ?></h2>
                                                    </div>
                                                    <div class="flex-grow sm:mb-0 inline-flex overflow-hidden items-center flex justify-end	">
                                                        <h2 class="sm:text-base text-base title-font text-white">$<span id="subTotalPrice-<?= $keyItemIndex ?>"><?= $items['totalSalesPrice'] ?></span></h2>
                                                    </div>
                                                    <a href="javascript:void(0)" style="color: white" class="ml-7"> <i class="fa fa-trash cart_delete" data-cart-id="<?= $cartId ?>" data-product-id="<?= $getSingleProduct['product_id'] ?>" data-variations-id="<?= $items['variationsId'] ?>"></i></a>
                                                </div>
                                                <div>
                                                    <div class="overflow-hidden items-center mt-4">
                                                        <?php if ($items['ModifierSet']) { ?>
                                                            <?php
                                                            $data = '';
                                                            $priceModifiers = 0;
                                                            $itemModifiersCheckbox = (array)$items['ModifiersItemCheckbox'];
                                                            foreach ($itemModifiersCheckbox as $key => $itemsSelected) {
                                                                $itemsSelected = (array)$itemsSelected;
                                                                if (isset($itemsSelected['modifiers_id'])) {
                                                                    $priceModifiers += $itemsSelected['modifiers_price'];
                                                                    $data .= $itemsSelected['modifier_name'] . ' - ' . '$' . $itemsSelected['modifiers_price'] . ' , ';
                                                                }
                                                            }

                                                            ?>
                                                            <h2 class="sm:text-base text-base title-font text-white">$<span id="originalPrice-<?= $keyItemIndex ?>"><?= $getSingleProduct['v_sale_price'] ?></span></h2>
                                                            <div class="text-white subtitle-font text-lg mt-4">
                                                                <p>Size: <?= $getSingleProduct['v_name'] ?></p>
                                                            </div>
                                                            <div class="text-white subtitle-font text-lg mt-4">
                                                                <p>Modifier: <?= $data ?></p>
                                                                <?php if ($items['Note']) { ?>
                                                                    <p class="mt-4">Note: <?= $items['Note'] ?></p>
                                                                <?php } ?>
                                                            </div>
                                                        <?php } else { ?>
                                                            <h2 class="sm:text-base text-base title-font text-white">$<span id="originalPrice-<?= $keyItemIndex ?>"><?= $getSingleProduct['v_sale_price'] ?></span></h2>
                                                            <div class="text-white subtitle-font text-lg mt-4">
                                                                <p>Size: <?= $getSingleProduct['v_name'] ?></p>
                                                            </div>
                                                        <?php } ?>

                                                        <input type="hidden" value="<?=$maxQuantity?>" id="maxInputQty-<?= $keyItemIndex ?>">

                                                        <div class="qtydiv w-2/12 mt-4">
                                                            <div class="qtybox flex" style="border: 2px solid white;">
                                                                <span class="btnqty qtyminus icon icon-minus" style="color: white; padding:13px; cursor:pointer" data-index="<?= $keyItemIndex ?>">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                                        <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd" />
                                                                    </svg>
                                                                </span>
                                                                <input type="text" id="quantity" style="border: none !important; margin:0px !important; text-align:center " name="CartDetails[<?=$indexItem?>][quantity]" value="<?= $items['quantity'] ?>" min="1" class="quantity-selector quantity-input select-quantity-<?= $keyItemIndex ?>" readonly="">
                                                                <input type="hidden" name="CartDetails[<?=$indexItem?>][maxqty]" value="<?=$maxQuantity?>">
                                                                <span class="btnqty qtyplus icon icon-plus" style="color: white; padding:13px; cursor:pointer" data-index="<?= $keyItemIndex ?>"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                                        <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                            <span id="QuantityErrorMessage-<?= $keyItemIndex ?>" style="color: red"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <div class="lg:w-1/3 md:w-1/2 flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
                            <div>
                                <div class="p-4 ">
                                    <h2 class="sm:text-base text-base title-font text-white mb-4">Bill Details</h2>
                                    <div class="flex py-2">
                                        <span class="text-white subtitle-font text-lg">Item total</span>
                                        <span class="ml-auto text-white subtitle-font text-lg">$<?= $suntotal ?></span>
                                    </div>
                                    <div class="flex">
                                        <span class="sm:text-xl mt-6 text-xl title-font text-white mb-4">Subtotal</span>
                                        <span class=" ml-auto sm:text-xl mt-6 text-xl title-font text-white mb-4">$<?= $suntotal ?></span>
                                    </div>
                                    <div class="mt-6" id="checkOutContent">
                                        <a href="checkout.php" class="checkout-button mt-6">CHECKOUT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
        </div>
    </form>
<script>
    $(document).on('click', '.cart_delete', function(e) {
        e.preventDefault();
        var productId = $(this).data('product-id');
        var cart_id = $(this).data('cart-id');
        var variationsId = $(this).data('variations-id');

        if (confirm("Are you want to delete It ?")) {
            $.ajax({
                url: '<?php echo $baseUrl; ?>app/delete_product.php',
                data: {
                    productId: productId,
                    variationsId: variationsId,
                    cart_id: cart_id,
                },
                type: "POST",
                success: function(response) {
                    if (response == 1) {
                        location.reload();
                    }
                },
                error: function() {
                    alert('Somethings Wrong');
                },
                beforeSend: function() {}
            });

        }
        return false;
    });

    $('.qtybox .btnqty').on('click', function() {
        var qty = parseInt($(this).parent('.qtybox').find('.quantity-input').val());
        if ($(this).hasClass('qtyplus')) {
            qty++;
        } else {
            if (qty > 1) {
                qty--;
            }
        }
        qty = (isNaN(qty)) ? 1 : qty;
        $(this).parent('.qtybox').find('.quantity-input').val(qty);
    });

    $(document).off('click', 'span.btnqty.qtyplus.icon.icon-plus,span.btnqty.qtyminus.icon.icon-minus').on('click', 'span.btnqty.qtyplus.icon.icon-plus,span.btnqty.qtyminus.icon.icon-minus', function(event) {
         event.preventDefault();

           var currentIndx = $(this).data('index');
        // var salesPrice = $('span#originalPrice-' + currentIndx + '').html();
           var quantity = $('input.select-quantity-' + currentIndx + '').val();
        // var totalSalesPrice = parseInt(salesPrice * quantity);
        // totalSalesPrice = (isNaN(totalSalesPrice)) ? 1 : totalSalesPrice;
        // $('span#subTotalPrice-' + currentIndx + '').html(totalSalesPrice);
        //
         var maxQuantity = $('input#maxInputQty-' + currentIndx + '').val();


         if (quantity>maxQuantity){
            $('span#QuantityErrorMessage-'+currentIndx+'').html("Quantity Exceed");
            $(this).addClass('disbaleItem');
         }else{
             $(this).next().next().removeClass('disbaleItem');
             $('span#QuantityErrorMessage-'+currentIndx+'').html("");
         }

        setTimeout(
            function()
            {
                $('span#QuantityErrorMessage-'+currentIndx+'').html("");
            },5000
        );
        if (quantity>maxQuantity) {
            var html = '<button type="button" value="updated" >Updated</button>';
            $('div#checkOutContent').html(html);
        }else{
            var html = '<button type="submit" value="updated" >Updated</button>';
            $('div#checkOutContent').html(html);
        }
    });
</script>
<?php
include "includes/footer.php";
?>