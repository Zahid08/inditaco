<?php
session_start();
include "includes/header.php";
?>
<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
    <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
        <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font mb-4 mt-3 text-6xl text-white">Contact Us</h1>
            <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Lorem Ipsum some tagline about us or our story</p>
        </div>
    </div>
</section>
<div class="bg-texture">
    <section class=" body-font">
        <div class="container px-8 py-16 mb-16 mx-auto">
            <section class="text-gray-600 body-font relative">
                <div class="container px-5 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
                    <div class="lg:w-2/3 md:w-1/2 bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-start relative">
                        <iframe  class="absolute inset-0" frameborder="0" title="map" marginheight="0" marginwidth="0" scrolling="no" src="https://snazzymaps.com/embed/326636" width="100%" height="100%" style="border:none;"></iframe>
                            <div class="bg-white relative flex flex-wrap py-6 rounded shadow-md">
                            <div class="lg:w-1/2 px-6">
                                <h2 class="title-font font-semibold text-gray-900 tracking-widest text-xs">ADDRESS</h2>
                                <p class="mt-1">Photo booth tattooed prism, portland taiyaki hoodie neutra typewriter</p>
                            </div>
                            <div class="lg:w-1/2 px-6 mt-4 lg:mt-0">
                                <h2 class="title-font font-semibold text-gray-900 tracking-widest text-xs">EMAIL</h2>
                                <a class="text-indigo-500 leading-relaxed">Eatinditaco@gmail.com </a>
                                <h2 class="title-font font-semibold text-gray-900 tracking-widest text-xs mt-4">PHONE</h2>
                                <p class="leading-relaxed">732.309.4719 </p>
                            </div>
                        </div>
                    </div>
                    <div class="lg:w-1/3 md:w-1/2 flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
                        <h2 class="text-xl text-white title-font mb-1 font-medium title-font">Feedback</h2>
                        <p class="leading-relaxed mb-5 subtitle-font text-white">Post-ironic portland shabby chic echo park, banjo fashion axe</p>
                        <div class="relative mb-4">                            
                            <input type="text" id="name" name="name" placeholder="Enter Name">
                        </div>
                        <div class="relative mb-4">                            
                            <input type="email" id="email" name="email" placeholder="Enter Email">
                        </div>
                        <div class="relative mb-4">                           
                            <textarea id="message" rows="4" placeholder="Type your Message..." name="message"></textarea>
                        </div>
                        <button class="cart-button" type="submit">Submit Message</button>                        
                    </div>
                </div>
            </section>
        </div>
    </section>

</div>

<?php
include "includes/footer.php";
?>