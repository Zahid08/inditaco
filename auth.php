<?php

include "includes/header.php";

?>
<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
    <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
        <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font mb-4 mt-3 text-6xl text-white">Login / Register</h1>

            <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Lorem Ipsum some tagline about us or our story</p>
        </div>
    </div>
</section>
<div class="bg-texture">
    <section class=" body-font">
        <div class="container px-36 py-16 mb-16 mx-auto">
            <section class="text-gray-600 body-font relative">
                <div class="container px-5 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
                    <div class="lg:w-1/2 md:w-1/2 flex flex-wrap flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0 p-20 md:border-r md:border-b-0 mb-10 md:mb-0 pb-10 border-b border-black">
                        <h2 class="text-xl text-white title-font mb-1 font-medium title-font">Login with us</h2>
                        <p class="leading-relaxed mb-5 subtitle-font text-white">Post-ironic portland shabby chic echo park, banjo fashion axe</p>
                        <section class="text-gray-600 body-font relative">
                            <form class="container mx-auto" id="login_form" method="POST">
                                <div class="mx-auto">
                                    <div class="flex flex-wrap -m-2">
                                        <div class="p-2 w-full">
                                            <div class="relative">
                                                <input type="text" id="email" class="register" name="email" placeholder="Enter Email Address">
                                            </div>
                                        </div>
                                        <div class="p-2 w-full">
                                            <div class="relative">
                                                <input type="password" id="password" class="register" name="password" placeholder="Enter Password">
                                            </div>
                                        </div>

                                        <div class="p-2 pt-4 w-full">
                                            <div class="relative">
                                            <button class="cart-button flex relative item-center" id="login_btn" name="submit" type="submit"> <i class="fa fa-circle-o-notch fa-spin me-1" id="loading_spinner"></i><span id="loading_spinner">&#160 &#160 &#160</span>Login</button>
                                    
                                            </div>
                                        </div>
                                        <div class="p-2 pt-4 w-full">
                                            <div class="relative">
                                                <a href="#" class="leading-relaxed mb-5 subtitle-font sc-color">Forget Password?</a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                    <div class="lg:w-1/2 md:w-1/2 flex flex-wrap flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0 p-20">
                        <h2 class="text-xl text-white title-font mb-1 font-medium title-font">Register with us</h2>
                        <p class="leading-relaxed mb-5 subtitle-font text-white">Post-ironic portland shabby chic echo park, banjo fashion axe</p>
                        <div class="alert alert-danger" role="alert" id="error">

                        </div>
                        <form id="register_form" method="post">
                            <section class="text-gray-600 body-font relative">
                                <div class="container mx-auto">
                                    <div class="mx-auto">
                                        <div class="flex flex-wrap -m-2">
                                            <div class="p-2 w-full">
                                                <div class="relative">
                                                    <input type="text" id="full_name" class="register" name="name" placeholder="Enter Full Name">
                                                </div>
                                            </div>
                                            <div class="p-2 w-full">
                                                <div class="relative">
                                                    <input type="email" id="register_email" class="register" name="email" placeholder="Enter Email Address">
                                                </div>
                                            </div>
                                            <div class="p-2 w-1/2">
                                                <div class="relative">
                                                    <input type="password" id="register_password" class="register" name="name" placeholder="Enter password">
                                                </div>
                                            </div>
                                            <div class="p-2 w-1/2">
                                                <div class="relative">
                                                    <input type="password" id="retype_password" class="register" name="name" placeholder="Re-enter password">
                                                </div>
                                            </div>
                                            <div class="p-2 w-1/2">
                                                <div class="relative flex radio">
                                                    <label class="radio_button">
                                                        <p style="line-height: 25px;">Male</p>
                                                        <input type="radio" checked="checked" id="male" name="gender" value="Male">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="p-2 w-1/2">
                                                <div class="relative flex radio">
                                                    <label class="radio_button">
                                                        <p style="line-height: 25px;">Female</p>
                                                        <input type="radio" id="female" name="gender" value="Female">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="p-2 w-full">
                                                <p class="leading-relaxed date_of_birth subtitle-font text-white">Date of Birth</p>
                                            </div>
                                            <div class="p-2 w-1/3">
                                                <div class="relative flex">
                                                    <select class="combine" id="date" name="day">
                                                        <option select value="">Day</option>
                                                        <option value="01">1</option>
                                                        <option value="02">2</option>
                                                        <option value="03">3</option>
                                                        <option value="04">4</option>
                                                        <option value="05">5</option>
                                                        <option value="06">6</option>
                                                        <option value="07">7</option>
                                                        <option value="08">8</option>
                                                        <option value="09">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                        <option value="13">13</option>
                                                        <option value="14">14</option>
                                                        <option value="15">15</option>
                                                        <option value="16">16</option>
                                                        <option value="17">17</option>
                                                        <option value="18">18</option>
                                                        <option value="19">19</option>
                                                        <option value="20">20</option>
                                                        <option value="21">21</option>
                                                        <option value="22">22</option>
                                                        <option value="23">23</option>
                                                        <option value="24">24</option>
                                                        <option value="25">25</option>
                                                        <option value="26">26</option>
                                                        <option value="27">27</option>
                                                        <option value="28">28</option>
                                                        <option value="29">29</option>
                                                        <option value="30">30</option>
                                                        <option value="31">31</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="p-2 w-1/3">
                                                <div class="relative flex">
                                                    <select class="combine" id="month" name="month">
                                                        <option select value="">Month</option>
                                                        <option value="01">January</option>
                                                        <option value="02">February</option>
                                                        <option value="03">March</option>
                                                        <option value="04">April</option>
                                                        <option value="05">May</option>
                                                        <option value="06">June</option>
                                                        <option value="07">July</option>
                                                        <option value="08">August</option>
                                                        <option value="09">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="p-2 w-1/3">
                                                <div class="relative flex">
                                                    <select class="combine" id="year" name="year">
                                                        <option selected value="">Year</option>
                                                        <option value="2005">2005</option>
                                                        <option value="2004">2004</option>
                                                        <option value="2003">2003</option>
                                                        <option value="2002">2002</option>
                                                        <option value="2001">2001</option>
                                                        <option value="2000">2000</option>
                                                        <option value="1999">1999</option>
                                                        <option value="1998">1998</option>
                                                        <option value="1997">1997</option>
                                                        <option value="1996">1996</option>
                                                        <option value="1995">1995</option>
                                                        <option value="1994">1994</option>
                                                        <option value="1993">1993</option>
                                                        <option value="1992">1992</option>
                                                        <option value="1991">1991</option>
                                                        <option value="1990">1990</option>
                                                        <option value="1989">1989</option>
                                                        <option value="1988">1988</option>
                                                        <option value="1987">1987</option>
                                                        <option value="1986">1986</option>
                                                        <option value="1985">1985</option>
                                                        <option value="1984">1984</option>
                                                        <option value="1983">1983</option>
                                                        <option value="1982">1982</option>
                                                        <option value="1981">1981</option>
                                                        <option value="1980">1980</option>
                                                        <option value="1979">1979</option>
                                                        <option value="1978">1978</option>
                                                        <option value="1977">1977</option>
                                                        <option value="1976">1976</option>
                                                        <option value="1975">1975</option>
                                                        <option value="1974">1974</option>
                                                        <option value="1973">1973</option>
                                                        <option value="1972">1972</option>
                                                        <option value="1971">1971</option>
                                                        <option value="1970">1970</option>
                                                        <option value="1969">1969</option>
                                                        <option value="1968">1968</option>
                                                        <option value="1967">1967</option>
                                                        <option value="1966">1966</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="text" id="date_of_birth" name="date_of_birth" hidden value="" />
                                            <div class="p-2 w-full">
                                                <p class="leading-relaxed subtitle-font text-lg text-white ">By Clicking On Register Button you are agreeing with our Privacy Policy and Terms & Condition</p>
                                            </div>
                                            <div class="p-2 pt-4 w-1/2">
                                                <div class="relative">
                                                    <button class="cart-button flex relative item-center" id="register_btn" name="submit" type="submit"> <i class="fa fa-circle-o-notch fa-spin me-1" id="loading_spinner"></i><span id="loading_spinner">&#160 &#160 &#160</span>Register</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<script>
    $(document).ready(function() {

        $('.combine').on('change', function() {
            var date = $('#date').val() + '/' + $('#month').val() + '/' + $('#year').val();
            $('#date_of_birth').val(date);
        });

        $('#register_form').submit(function(e) {

            e.preventDefault();

            var full_name = $('#full_name').val();
            var email = $('#register_email').val();
            var password = $('#register_password').val();
            var retype_password = $('#retype_password').val();
            var date_of_birth = $('#date_of_birth').val();
            var gender = document.querySelector('input[name="gender"]:checked').value;
            var type = 1;
            var submit = document.getElementById("register_btn").name;

            if (full_name != "" && email != "" && password != "" && retype_password != "" && date_of_birth != "" && gender != "") {

                $("#loading_spinner").css({
                    "display": "block",
                    "padding": "4px"
                });

                mydata = {
                    type: type,
                    full_name: full_name,
                    email: email,
                    password: password,
                    retype_password: retype_password,
                    date_of_birth: date_of_birth,
                    gender: gender,
                    submit: submit
                };


                $.ajax({
                    url: "app/authentication.php",
                    type: "POST",
                    data: JSON.stringify(mydata),

                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        if (dataResult.statusCode == 200) {
                            alert('Something Went Wrong');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 201) {
                            alert('Password is Not Match ');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 202) {
                            alert('Email is Not Valid');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 203) {
                            alert('Email Already Exist Please Login');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 207) {
                            alert('You are Register Now Please Login to Access your Panel');
                            window.location.href = "auth.php";
                            $("#register_form")[0].reset();
                        } else if (dataResult.statusCode == 205) {
                            alert('STMT FAILED');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        }
                    }
                });

            } else {
                alert('Please fill all the field !');
            }
        });

        $('#login_form').submit(function(e) {

            e.preventDefault();            
            var email = $('#email').val();
            var password = $('#password').val();           
            var type = 2;
            var submit = document.getElementById("login_btn").name;


            if (email != "" && password != "") {

                $("#loading_spinner").css({
                    "display": "block",
                    "padding": "4px"
                });

                mydata = {
                    type: type,                    
                    email: email,
                    password: password,                    
                    submit: submit
                };


                $.ajax({
                    url: "app/authentication.php",
                    type: "POST",
                    data: JSON.stringify(mydata),

                    success: function(dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        if (dataResult.statusCode == 200) {
                            alert('Something Went Wrong');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 201) {
                            alert('Email is Not Exist');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 202) {
                            alert('Password is Wrong');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 205) {
                            alert('STMT FAILED');
                            $("#loading_spinner").css({
                                "display": "none"
                            });
                        } else if (dataResult.statusCode == 206) {
                                                        
                            window.location.href = "index.php";
                            $("#login_form")[0].reset();
                                                        
                        }
                    }
                });

            } else {
                alert('Please fill all the field !');
            }
        });

    });
</script>
<?php
include "includes/footer.php";
?>