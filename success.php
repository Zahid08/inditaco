<?php
include "includes/header.php";
?>
<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
    <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
        <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font mb-4 mt-3 text-6xl text-white">Payment Status</h1>
            <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Lorem Ipsum some tagline about us or our story</p>
        </div>
    </div>
</section>

<div style="background-color: #0c0c0c;">
    <section class="text-gray-600 body-font relative">
        <div class="container px-5 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
            <div class="lg:w-2/3 md:w-1/2 ">
                <h1 class="title-font mb-4 mt-3 text-6xl text-green">Successfully Paid</h1>
                <h1 class="title-font mb-4 mt-3 text-6xl text-white">Invoice Id : <?=isset($_GET['Invoice'])?$_GET['Invoice']:''?></h1>
            </div>
            <div class="mt-6" id="checkOutContent">
                <a href="shop.php" class="checkout-button mt-6">BUY AGAIN</a>
            </div>

        </div>
    </section>

</div>
<?php
include "includes/footer.php";
?>
