<?php
include "includes/header.php";
require('../inditaco/app/cart_details.php');
require('../inditaco/app/connection.inc.php');
$cartItemsList = getCartItems($conn, $bid);

$addressDetails='';
if (isset($_SESSION['user'])) {
    $addressDetails = getShippingAddress($conn, $bid);
}
?>
<style>
    div#selectedAddressItem {
        cursor: pointer;
    }
    div.selected {
        border: 1px solid gray;
    }
</style>
<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
   <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
      <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
         <h1 class="title-font mb-4 mt-3 text-6xl text-white">Cart</h1>
         <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Lorem Ipsum some tagline about us or our story</p>
      </div>
   </div>
</section>
<div style="background-color: #0c0c0c;">
   <section class="text-gray-600 body-font  relative">
      <div class="container px-10 py-24 mx-auto flex sm:flex-nowrap flex-wrap">
         <div class="lg:w-7/12 md:w-1/2 ">
             <?php
                if (isset($_SESSION['user'])) {
             ?>
            <div class="p-4 ">
               <div class=" border-2 rounded-lg border-black border-opacity-50 p-8 ">
                  <div class="flex sm:flex-row flex-col">
                     <div class="flex-grow inline-flex overflow-hidden items-center">
                        <div>
                           <h2 class="sm:text-base text-base title-font text-white mb-4">Choose a delivery address
                           </h2>
                           <div class="sm:text-base text-base title-font flex text-white my-4">
                              <p class="mr-4 subtitle-font text-sm">To place your order now, log in to your existing account or sign up.</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="container mt-4 mx-auto">
                     <div class="flex flex-wrap -m-4">
                         <?php if ($addressDetails){

                             foreach ($addressDetails as $key=>$itemsAddress) {?>
                            <div class="p-4 md:w-1/2">
                               <div class="flex rounded-lg h-full border-black border-2 p-8 flex-col" data-address-id="<?=$itemsAddress['address_id']?>" id="selectedAddressItem">
                                  <div class="flex items-center mb-3">
                                     <h2 class="sm:text-base text-base title-font text-white"><?=$itemsAddress['area']?></h2>
                                  </div>
                                  <div class="flex-grow">
                                     <p class="text-white subtitle-font text-lg"><?=$itemsAddress['address']?></p>
                                     <div class="flex-grow mt-4 inline-flex overflow-hidden items-center flex justify-end	">
                                        <button class="cart-button item-end mr-4" type="submit">Deliver Here</button>
                                     </div>
                                  </div>
                               </div>
                            </div>
                         <?php }

                         } ?>

                         <div class="p-4 md:w-full" id="addNewArea">
                             <div class="flex items-center mb-3">
                                 <button class="cart-button item-end mr-4" type="button" id="AddnewAddress">Add New</button>
                             </div>
                         </div>

                        <div class="p-4 md:w-full" id="addNewBlock">
                           <div class="flex rounded-lg h-full border-black border-2 p-8 flex-col">
                              <div class="flex items-center mb-3">
                                 <h2 class="sm:text-base text-base title-font text-white">Add New Address</h2>
                              </div>
                              <form id="register_form" method="post"  action="<?= $baseUrl ?>app/create_new_address.php">
                        <section class="text-gray-600 body-font relative">
                            <div class="container mx-auto">
                                <div class="mx-auto">
                                    <div class="flex flex-wrap -m-2">
                                        <div class="p-2 w-full">
                                            <div class="relative">
                                                <input type="text" id="full_name" class="register" name="first_line_address" placeholder="First Line Address" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-full">
                                            <div class="relative">
                                                <input type="text" id="register_email" class="register" name="second_line_address" placeholder="Second Line" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-1/2">
                                            <div class="relative">
                                                <input type="text" id="register_password" class="register" name="Area" placeholder="Area" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-1/2">
                                            <div class="relative">
                                                <input type="text" id="retype_password" class="register" name="City" placeholder="City" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-1/2">
                                            <div class="relative">
                                                <input type="text" id="retype_password" class="register" name="State" placeholder="State" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-1/2">
                                            <div class="relative">
                                                <input type="text" id="retype_password" class="register" name="Pincode" placeholder="Pincode" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-1/2">
                                            <div class="relative">
                                                <input type="text" id="retype_password" class="register" name="Landmark" placeholder="Landmark" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-1/2">
                                            <div class="relative">
                                                <input type="text" id="retype_password" class="register" name="Country" placeholder="Country" required>
                                            </div>
                                        </div>
                                        <div class="p-2 w-full">
                                            <p class="leading-relaxed subtitle-font text-lg text-white ">By Clicking On Register Button you are agreeing with our Privacy Policy and Terms & Condition</p>
                                        </div>
                                        <div class="p-2 pt-4 w-1/2">
                                            <div class="relative">
                                                <button class="cart-button flex relative item-center" id="register_btn" name="submit" type="submit"> Add Address</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>
                        </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

             <div class="p-4 " id="paymentAreaBlock">

                 <form id="register_form" method="post"  action="<?= $baseUrl ?>app/payment.php">
                 <div class=" border-2 rounded-lg border-black border-opacity-50 p-8 ">

                     <div class="flex sm:flex-row flex-col">
                         <div class="flex-grow inline-flex overflow-hidden items-center">
                             <div>
                                 <h2 class="sm:text-base text-base title-font text-white mb-4">Choose Payment Options</h2>
                             </div>
                         </div>
                     </div>

                     <div class="container mt-4 mx-auto">
                         <div class="flex flex-wrap">
                             <input type="hidden" name="AddressId" id="addressId" value="">
                             <input name="PaymentOptions" class="checkbox" type="radio" value="cash_on_delivery" checked> Cash On Delivery
                             <textarea class="w-full	mt-4 subtitle-font" placeholder="Add Note, We will take care" name="notes"></textarea>
                         </div>
                     </div>

                     <div class="p-2 pt-4 w-1/2">
                         <div class="relative">
                             <button class="cart-button flex relative item-center" id="register_btn" name="submit" type="submit">Pay Now</button>
                         </div>
                     </div>
                 </div>
                 </form>
             </div>

            <?php }else{ ?>
                    <div class="p-4">
                        <div class="flex border-2 rounded-lg border-black border-opacity-50 p-8 sm:flex-row flex-col">
                            <div class="flex-grow inline-flex overflow-hidden items-center">
                                <div>
                                    <h2 class="sm:text-base text-base title-font text-white mb-4">Account</h2>
                                    <div class="sm:text-base text-base title-font flex text-white my-4">
                                        <p class="mr-4 subtitle-font text-sm">To place your order now, log in to your existing account or sign up.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-grow sm:mb-0 mb-4 inline-flex overflow-hidden items-center flex justify-end	">
                                <a href="auth.php" class="cart-button item-end mr-4" type="submit">LOGIN / REGISTER</a>
                            </div>
                        </div>
                    </div>

                    <div class="p-4">
                        <div class="flex border-2 rounded-lg border-black border-opacity-50 p-8 sm:flex-row flex-col">
                            <div class="flex-grow inline-flex overflow-hidden items-center">
                                <div>
                                    <h2 class="sm:text-base text-base title-font text-white mb-4">Delivery address</h2>
                                    <div class="sm:text-base text-base title-font flex text-white my-4">
                                        <p class="mr-4 subtitle-font text-sm">To place your order now, log in to your existing account or sign up.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="p-4">
                        <div class="flex border-2 rounded-lg border-black border-opacity-50 p-8 sm:flex-row flex-col">
                            <div class="flex-grow inline-flex overflow-hidden items-center">
                                <div>
                                    <h2 class="sm:text-base text-base title-font text-white mb-4">PAYMENT</h2>
                                    <div class="sm:text-base text-base title-font flex text-white my-4">
                                        <p class="mr-4 subtitle-font text-sm">To place your order now, log in to your existing account or sign up.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              <?php  } ?>
         </div>
         <div class="lg:w-5/12 md:w-1/2 flex flex-col md:ml-auto w-full md:py-8 mt-8 md:mt-0">
            <div>
               <h2 class="sm:text-base text-base title-font p-4 pt-0 text-white mb-4">Your Order</h2>
                <?php
                $suntotal = 0;
                if ($cartItemsList) {
                foreach ($cartItemsList as $keyItemIndex => $items) {
                $maxQuantity=$items['maxQuantity'];
                $suntotal += $items['totalSalesPrice'];
                $getSingleProduct = getSingleProduct($items['productid'], $items['variationsId'], $bid, $conn);
                if (isset($_SESSION['user'])) {
                    $cartId = $keyItemIndex;
                    $indexItem=$keyItemIndex;
                } else {
                    $cartId = $items['cart_id'];
                    $indexItem=$keyItemIndex;
                }
                ?>
                <div class="p-4 py-6 border-b-2 border-black">
                  <div class="flex sm:flex-row flex-col">
                     <div class="flex-grow inline-flex overflow-hidden items-center">
                        <h2 class="sm:text-base text-base title-font text-white"><?= $getSingleProduct['product_name'] ?></h2>
                     </div>
                     <div class="flex-grow sm:mb-0 inline-flex overflow-hidden items-center flex justify-end	">
                        <h2 class="sm:text-base text-base title-font text-white">$<?= $items['totalSalesPrice'] ?></h2>
                     </div>
                  </div>
                  <div>
                     <div class="overflow-hidden items-center mt-4">
                         <h2 class="sm:text-base text-base title-font text-white">$<span id="originalPrice-<?= $keyItemIndex ?>"><?= $getSingleProduct['v_sale_price'] ?> <i class="fa fa-times"></i> <?= $items['quantity'] ?> </span></h2>
                    <?php if ($items['ModifierSet']) { ?>
                        <?php
                        $data = '';
                        $priceModifiers = 0;
                        $itemModifiersCheckbox = (array)$items['ModifiersItemCheckbox'];
                        foreach ($itemModifiersCheckbox as $key => $itemsSelected) {
                            $itemsSelected = (array)$itemsSelected;
                            if (isset($itemsSelected['modifiers_id'])) {
                                $priceModifiers += $itemsSelected['modifiers_price'];
                                $data .= $itemsSelected['modifier_name'] . ' - ' . '$' . $itemsSelected['modifiers_price'] . ' , ';
                            }
                        }

                        ?>
                        <div class="text-white subtitle-font text-lg mt-4">
                           <p>Modifier: <?= $data ?></p>
                            <?php if ($items['Note']) { ?>
                                <p class="mt-4">Note: <?= $items['Note'] ?></p>
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                        <div class="text-white subtitle-font text-lg mt-4">
                            <p>Size: <?= $getSingleProduct['v_name'] ?></p>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
               </div>
                <?php  } ?>

               <div class="p-4 pt-6">
                  <h2 class="sm:text-base text-base title-font text-white mb-4">Bill Details</h2>
                  <div class="flex py-2">
                     <span class="text-white subtitle-font text-lg">Sub total</span>
                     <span class="ml-auto text-white subtitle-font text-lg">$<?=$suntotal?></span>
                  </div>
                  <div class="flex py-2">
                     <span class="text-white subtitle-font text-lg">Delivery Partner</span>
                     <span class="ml-auto text-white subtitle-font text-lg">0.00</span>
                  </div>
                  <div class="flex py-2">
                     <span class="text-green-900 subtitle-font text-lg">Total Discount</span>
                     <span class="ml-auto text-green-900 subtitle-font text-lg">$0.00</span>
                  </div>
                  <div class="flex py-2">
                     <span class="text-white subtitle-font text-lg">Taxes and Charges</span>
                     <span class="ml-auto text-white subtitle-font text-lg">$0.00</span>
                  </div>
                  <div class="flex">
                     <span class="sm:text-xl mt-6 text-xl title-font text-white mb-4">TO PAY</span>
                     <span class=" ml-auto sm:text-xl mt-6 text-xl title-font text-white mb-4">$<?php echo $suntotal ?></span>
                  </div>
               </div>
                <?php  } ?>
            </div>
         </div>
      </div>
   </section>
</div>
<script>
    $('div#addNewBlock').hide();
    $('div#paymentAreaBlock').hide();

    $(document).on('click', 'button#AddnewAddress', function(e) {
        e.preventDefault();
        $('div#addNewArea').hide();
        $('div#addNewBlock').show();
    });

    $(document).on('click', '#selectedAddressItem', function(e) {
        e.preventDefault();
       var addressId=$(this).data('address-id');
        $('div#selectedAddressItem').removeClass('selected');
        $(this).addClass('selected');
        $('input#addressId').val(addressId);

        $('div#paymentAreaBlock').show();
    });

</script>
<?php
include "includes/footer.php";
?>