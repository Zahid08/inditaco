<?php
    require('connection.inc.php');

    session_start();

	$output = array('error'=>false);
	$OriginalPrice                  =isset($_POST['OriginalPrice'])?$_POST['OriginalPrice']:'';
	$notes                             =isset($_POST['notes'])?$_POST['notes']:'';
	$id                             =isset($_POST['productid'])?$_POST['productid']:'';
	$quantity                       = isset($_POST['quantity'])?$_POST['quantity']:'';
	$variationsId                   =isset($_POST['variationsId'])?$_POST['variationsId']:'';
	$totalSalesPrice                = $_POST['totalSalesPrice'];
    $ModifierSet                   =isset($_POST['ModifierSet'])?$_POST['ModifierSet']:'';
    $ModifiersItemCheckbox         =isset($_POST['ModifiersItemCheckbox'])?$_POST['ModifiersItemCheckbox']:array();

    $type                           =isset($_REQUEST['type'])?1:'';
    $postRadioSelectedItems         =isset($_POST['RaidoModifiersId'])?$_POST['RaidoModifiersId']:'';
    $ModifiersItemCheckboxRadio     =[];

    $maxQuantity                    = isset($_POST['Maxqty'])?$_POST['Maxqty']:'';

    //Validate
    if ($quantity>$maxQuantity){

        if ($type==1){
            $Url=$baseUrl.'shop.php';
            $_SESSION['message'] = 'Order Quantity Exceed';
            header('Location: '.$Url.'');
            exit();
        }

        $output['error'] = true;
        $_SESSION['message'] = 'Order Quantity Exceed';
        print_r(json_encode($output));
        exit();
    }

    if ($postRadioSelectedItems){
       foreach ($postRadioSelectedItems as $key=>$item) {
           $ModifiersItemCheckboxRadio[] = $_POST['ModifiersItemCheckboxRadio'][$item['modifiers_id']];
       }
    }

	if(isset($_SESSION['user'])){

        $data['productid']                      = $id;
        $data['OriginalPrice']                  = $OriginalPrice;
        $data['quantity']                       = $quantity;
        $data['maxQuantity']                    = $maxQuantity;
        $data['variationsId']                   = $variationsId;
        $data['totalSalesPrice']                = $totalSalesPrice;
        $data['ModifierSet']                    = $ModifierSet;
        $data['Note']                           = $notes;


        $note	   			                    =!empty($notes)?$notes:'';

        $modifiersEncodedItem   =array();
        $mpdifiersItemSelected='';

        if ($type==1) {

            $data['ModifiersItemCheckbox'] = array_merge($ModifiersItemCheckboxRadio, $ModifiersItemCheckbox);

            //For Validations Check
            $modifierIdListOne=[];
            foreach ($data['ModifiersItemCheckbox'] as $key=>$items){
                if (isset($items['modifiers_id'])){
                    array_push($modifierIdListOne, $items['modifiers_id']);
                }
            }
            $mpdifiersItemSelected=serialize($modifierIdListOne);
        }

            $content                                =json_encode($data);

            $useeId             =$_SESSION['id'];

            if ($type==1) {
                $getTableCartItemSql     	    = "SELECT * FROM cart_item where bid=$bid and uid=$useeId and product_id=$id and product_size_id=$variationsId and selected_modifiers='".$mpdifiersItemSelected."'";
            }else{
                $getTableCartItemSql     	    = "SELECT * FROM cart_item where bid=$bid and uid=$useeId and product_id=$id and product_size_id=$variationsId";
            }
            $resultItemsCartTableResult     = mysqli_query($conn, $getTableCartItemSql);

            if ($resultItemsCartTableResult->num_rows > 0) {
                $dataSqlItem         = mysqli_fetch_row($resultItemsCartTableResult);

                $previousQty    =$dataSqlItem[8];
                $cartItemId     =$dataSqlItem[0];
                $previousCarditem     =json_decode($dataSqlItem[13]);


                $modifierPrice=0;

                if ($type==1) {
                    foreach ($previousCarditem->ModifiersItemCheckbox as $key => $items) {
                        if (isset($items->modifiers_id)) {
                            $modifierPrice += $items->modifiers_price;
                        }
                    }
                }

                $newQty              =$previousQty+$quantity;
                $totalSalesPrice     = ($newQty*$OriginalPrice)+$modifierPrice;

                //Validations With New Qty
                if ($newQty>$maxQuantity){

                    if ($type==1){
                        $Url=$baseUrl.'shop.php';
                        $_SESSION['message'] = 'Order Quantity Exceed';
                        header('Location: '.$Url.'');
                        exit();
                    }


                    $output['error'] = true;
                    $output['message'] = 'Order Quantity Exceed';
                    print_r(json_encode($output));
                    exit();
                }

                $data['productid']                      = $previousCarditem->productid;
                $data['OriginalPrice']                  = $previousCarditem->OriginalPrice;
                $data['quantity']                       = $newQty;
                $data['maxQuantity']                    = $maxQuantity;
                $data['variationsId']                   = $previousCarditem->variationsId;
                $data['totalSalesPrice']                = $totalSalesPrice;
                $data['ModifierSet']                    = $previousCarditem->ModifierSet;
                $data['Note']                           = '';

                $content                                =json_encode($data);

                $sql = "UPDATE cart_item SET qty=$newQty,total_price=$totalSalesPrice,content='".$content."',note='".$notes."' WHERE id=$cartItemId";
                mysqli_query($conn, $sql);

            }else {
                $sqlInsert = "INSERT INTO cart_item (bid,uid,product_id, product_size_id ,price,total_price ,qty ,content,note,selected_modifiers) VALUES ($bid,$useeId,$id,$variationsId,$OriginalPrice,$totalSalesPrice,$quantity,'".$content."','".$note."','".$mpdifiersItemSelected."')";

                if (mysqli_query($conn, $sqlInsert)) {
                    $last_id = $conn->insert_id;
                    if (isset($data['ModifiersItemCheckbox'])) {
                        $modifiersList = $data['ModifiersItemCheckbox'];
                        foreach ($modifiersList as $key => $items) {
                            if (isset($items['modifiers_id'])) {
                                $modifierId = $items['modifiers_id'];
                                $price = $items['modifiers_price'];
                                $sqlCartModifiers = "INSERT INTO cart_modifier (cart_item_id, modifier_id, price ) VALUES ($last_id,$modifierId,$price)";
                                mysqli_query($conn, $sqlCartModifiers);
                            }
                        }
                    }
                }
            }

            $output['message'] = 'Item added to cart';

        if ($type==1){
            $Url=$baseUrl.'shop.php';
            header('Location: '.$Url.'');
            exit();
        }
	}
	else{

		if(!isset($_SESSION['cart'])){
			$_SESSION['cart'] = array();
		}

		$exist = array();
		$quantityArray=array();
		$variationsIdArray=array();

		$reloadProductArray=array();

		$sessionProductIndex=array();

		foreach($_SESSION['cart'] as $index=>$row){
			array_push($exist, $row['productid']);
			array_push($quantityArray, $row['quantity']);
			array_push($variationsIdArray, $row['variationsId']);
            $reloadProductArray[$row['variationsId']]      =$row;
            $sessionProductIndex[$row['variationsId']]     =$index;
		}
		

        $randomNumber =str_pad(rand(0, pow(10, 5)-1), 5, '0', STR_PAD_LEFT);

        $data['cart_id']                        = $randomNumber;
        $data['productid']                      = $id;
        $data['OriginalPrice']                  = $OriginalPrice;
        $data['quantity']                       = $quantity;
        $data['maxQuantity']                    = $maxQuantity;
        $data['variationsId']                   = $variationsId;
        $data['totalSalesPrice']                = $totalSalesPrice;
        $data['ModifierSet']                    = $ModifierSet;

        $modifiersEncodedItem=array();

        if ($type==1) {
            $data['ModifiersItemCheckbox']      = array_merge($ModifiersItemCheckboxRadio, $ModifiersItemCheckbox);

            //For Validations Check
            $modifierIdListOne=[];
            foreach ($data['ModifiersItemCheckbox'] as $key=>$items){
                if (isset($items['modifiers_id'])){
                    array_push($modifierIdListOne, $items['modifiers_id']);
                }
            }
            $modifiersEncodedItem[$variationsId] =serialize($modifierIdListOne);
        }

        $data['Note']                           = $notes;

        if (in_array($variationsId, $variationsIdArray) && $type !=1){

            $sessionIndex                           =$sessionProductIndex[$variationsId];
            $cartItems                             =$_SESSION['cart'][$sessionIndex] ;

            $getSessionQty                         =$cartItems['quantity'];
            $newQty                                =$getSessionQty+$quantity;

            //Validations With New Qty
            if ($newQty>$maxQuantity){

                if ($type==1){
                    $Url=$baseUrl.'shop.php';
                    $_SESSION['message'] = 'Order Quantity Exceed';
                    header('Location: '.$Url.'');
                    exit();
                }


                $output['error'] = true;
                $output['message'] = 'Order Quantity Exceed';
                print_r(json_encode($output));
                exit();
            }

            $data['quantity']                      = $newQty;
            $data['totalSalesPrice']               = $newQty*$OriginalPrice;
            $_SESSION['cart'][$sessionIndex]       =$data;

        }elseif (in_array($variationsId, $variationsIdArray) && $type ==1){

            //If Same Size Same Modifiers Then Updated Qty only
            //If Same size Different Modifiers Then Create New

            $reloadProduct=[];
            $modifierSessionIndex=[];

            //Set Modifiers ID
            foreach($_SESSION['cart'] as $index=>$row){
                $modifierIdArray=[];
                if ($row['ModifierSet']){
                    foreach ($row['ModifiersItemCheckbox'] as $key=>$itemsModifiers){
                        if (isset($itemsModifiers['modifiers_id'])){
                            array_push($modifierIdArray, $itemsModifiers['modifiers_id']);
                        }
                    }
                }
                $modifiersSelectedItemsTwo[$row['variationsId']][$index]  =serialize($modifierIdArray);
            }

            $matchItemKey='';
            foreach ($modifiersSelectedItemsTwo as $key=>$itemsArray){
               if ( array_key_exists($key,$modifiersEncodedItem)) {
                   $checkItems = $modifiersEncodedItem[$key];
                   if (in_array($checkItems,$itemsArray)){
                       $matchItemKey = array_search($checkItems, $itemsArray);
                       break;
                    }
               }
            }

            if ($matchItemKey || $matchItemKey==0){
                $cartItems                             =$_SESSION['cart'][$matchItemKey];

                $modifierPrice=0;
                foreach ($cartItems['ModifiersItemCheckbox'] as $key=>$items){
                    if (isset($items['modifiers_id'])){
                        $modifierPrice+=$items['modifiers_price'];
                    }
                }

                $getSessionQty                         =$cartItems['quantity'];
                $newQty                                =$getSessionQty+$quantity;
                $data['quantity']                      = $newQty;
                $data['totalSalesPrice']               = ($newQty*$OriginalPrice)+$modifierPrice;

                //Validations With New Qty
                if ($newQty>$maxQuantity){

                    if ($type==1){
                        $Url=$baseUrl.'shop.php';
                        $_SESSION['message'] = 'Order Quantity Exceed';
                        header('Location: '.$Url.'');
                        exit();
                    }

                    $output['error'] = true;
                    $output['message'] = 'Order Quantity Exceed';
                    print_r(json_encode($output));
                    exit();
                }

                $_SESSION['cart'][$matchItemKey]       =$data; //Updated Quantity In Same Size

            }else{
                if (array_push($_SESSION['cart'], $data)) {     //New Push
                    $output['message'] = 'Item added to cart';
                }
            }
        }else {
            if (array_push($_SESSION['cart'], $data)) {
                $output['message'] = 'Item added to cart';
            } else {
                $output['error'] = true;
                $output['message'] = 'Cannot add item to cart';
            }
        }

		if ($type==1){
		    $Url=$baseUrl.'shop.php';
            header('Location: '.$Url.'');
            exit();
        }
	}

	print_r(json_encode($output));
	exit();

?>