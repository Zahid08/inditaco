<?php
    require('connection.inc.php');

    session_start();

	$output = array('error'=>false);
	$OriginalPrice                  =isset($_POST['OriginalPrice'])?$_POST['OriginalPrice']:'';
	$notes                             =isset($_POST['notes'])?$_POST['notes']:'';
	$id                             =isset($_POST['productid'])?$_POST['productid']:'';
	$quantity                       = isset($_POST['quantity'])?$_POST['quantity']:'';
	$variationsId                   =isset($_POST['variationsId'])?$_POST['variationsId']:'';
	$totalSalesPrice                = $_POST['totalSalesPrice'];
    $ModifierSet                   =isset($_POST['ModifierSet'])?$_POST['ModifierSet']:'';
    $ModifiersItemCheckbox         =isset($_POST['ModifiersItemCheckbox'])?$_POST['ModifiersItemCheckbox']:array();

    $type                           =isset($_REQUEST['type'])?1:'';
    $postRadioSelectedItems         =isset($_POST['RaidoModifiersId'])?$_POST['RaidoModifiersId']:'';
    $ModifiersItemCheckboxRadio     =[];

    if ($postRadioSelectedItems){
        $key=$postRadioSelectedItems['modifiers_id'];
        $ModifiersItemCheckboxRadio[]=$_POST['ModifiersItemCheckboxRadio'][$key];
    }

	if(isset($_SESSION['user'])){

        $data['productid']                      = $id;
        $data['OriginalPrice']                  = $OriginalPrice;
        $data['quantity']                       = $quantity;
        $data['variationsId']                   = $variationsId;
        $data['totalSalesPrice']                = $totalSalesPrice;
        $data['ModifierSet']                    = $ModifierSet;
        $data['Note']                           = $notes;


        $note	   			                    =!empty($notes)?$notes:'';

        if ($type==1) {
            $data['ModifiersItemCheckbox'] = array_merge($ModifiersItemCheckboxRadio, $ModifiersItemCheckbox);
        }

        $content                                =json_encode($data);

        $useeId             =$_SESSION['id'];
        $cartItemsSql     	 = "SELECT * FROM cart_item where bid=$bid and uid=$useeId and product_id=$id and product_size_id=$variationsId and qty=$quantity";
        $resultItemsCart     = mysqli_query($conn, $cartItemsSql);

        if ($resultItemsCart->num_rows > 0) {
            $output['error'] = true;
            $output['message'] = 'Product already in cart';
        }else{

            $getTableCartItemSql     	    = "SELECT * FROM cart_item where bid=$bid and uid=$useeId and product_id=$id and product_size_id=$variationsId";
            $resultItemsCartTableResult     = mysqli_query($conn, $getTableCartItemSql);

            if ($resultItemsCartTableResult->num_rows > 0 && $type !=1) {
                $dataSqlItem         = mysqli_fetch_row($resultItemsCartTableResult);
                $ID=$dataSqlItem[0];
                $sql = "UPDATE cart_item SET product_size_id=$variationsId,total_price=$totalSalesPrice,qty=$quantity,content='".$content."',note='".$notes."' WHERE id=$ID";
                if (mysqli_query($conn, $sql)){

                    $sqlSubItemsDelete = "DELETE FROM cart_modifier WHERE cart_item_id=$ID";
                    mysqli_query($conn, $sqlSubItemsDelete);

                    if (isset($data['ModifiersItemCheckbox'])) {
                        $modifiersList = $data['ModifiersItemCheckbox'];
                        foreach ($modifiersList as $key => $items) {
                            if (isset($items['modifiers_id'])) {
                                $modifierId = $items['modifiers_id'];
                                $price = $items['modifiers_price'];
                                $sqlCartModifiers = "INSERT INTO cart_modifier (cart_item_id, modifier_id, price ) VALUES ($ID,$modifierId,$price)";
                                mysqli_query($conn, $sqlCartModifiers);
                            }
                        }
                    }
                }
            }else {
                $sqlInsert = "INSERT INTO cart_item (bid,uid,product_id, product_size_id ,price,total_price ,qty ,content,note) VALUES ($bid,$useeId,$id,$variationsId,$OriginalPrice,$totalSalesPrice,$quantity,'".$content."','".$note."')";

                if (mysqli_query($conn, $sqlInsert)) {
                    $last_id = $conn->insert_id;
                    if (isset($data['ModifiersItemCheckbox'])) {
                        $modifiersList = $data['ModifiersItemCheckbox'];
                        foreach ($modifiersList as $key => $items) {
                            if (isset($items['modifiers_id'])) {
                                $modifierId = $items['modifiers_id'];
                                $price = $items['modifiers_price'];
                                $sqlCartModifiers = "INSERT INTO cart_modifier (cart_item_id, modifier_id, price ) VALUES ($last_id,$modifierId,$price)";
                                mysqli_query($conn, $sqlCartModifiers);
                            }
                        }
                    }
                }
            }

            $output['message'] = 'Item added to cart';
        }

        if ($type==1){
            $Url=$baseUrl.'shop.php';
            header('Location: '.$Url.'');
            exit();
        }
	}
	else{

		if(!isset($_SESSION['cart'])){
			$_SESSION['cart'] = array();
		}

		$exist = array();
		$quantityArray=array();
		$variationsIdArray=array();

		$reloadProductArray=array();

		$sessionProductIndex=array();

		foreach($_SESSION['cart'] as $index=>$row){
			array_push($exist, $row['productid']);
			array_push($quantityArray, $row['quantity']);
			array_push($variationsIdArray, $row['variationsId']);
            $reloadProductArray[$row['variationsId']]      =$row;
            $sessionProductIndex[$row['variationsId']]     =$index;
		}
		

        $randomNumber =str_pad(rand(0, pow(10, 5)-1), 5, '0', STR_PAD_LEFT);

        $data['cart_id']                        = $randomNumber;
        $data['productid']                      = $id;
        $data['OriginalPrice']                  = $OriginalPrice;
        $data['quantity']                       = $quantity;
        $data['variationsId']                   = $variationsId;
        $data['totalSalesPrice']                = $totalSalesPrice;
        $data['ModifierSet']                    = $ModifierSet;

        $modifiersEncodedItem=array();

        if ($type==1) {
            $data['ModifiersItemCheckbox']      = array_merge($ModifiersItemCheckboxRadio, $ModifiersItemCheckbox);
            $modifiersEncodedItem =serialize($data['ModifiersItemCheckbox'] );
        }

        $data['Note']                           = $notes;

        if (in_array($variationsId, $variationsIdArray) && $type !=1){

            $sessionIndex                           =$sessionProductIndex[$variationsId];
            $cartItems                             =$_SESSION['cart'][$sessionIndex] ;

            $getSessionQty                         =$cartItems['quantity'];
            $newQty                                =$getSessionQty+$quantity;
            $data['quantity']                      = $newQty;
            $data['totalSalesPrice']               = $newQty*$OriginalPrice;
            $_SESSION['cart'][$sessionIndex]       =$data;

        }elseif (in_array($variationsId, $variationsIdArray) && $type ==1){

            foreach($_SESSION['cart'] as $index=>$row){
               if (isset($row['ModifiersItemCheckbox'])){
                   $modifiersSelectedItemsTwo[serialize($row['ModifiersItemCheckbox'])]     =$index;
               }
            }


            $sessionIndex                           =$modifiersSelectedItemsTwo[$modifiersEncodedItem];
            $cartItems                             =$_SESSION['cart'][$sessionIndex] ;

            echo "<pre>";
            print_r($modifiersSelectedItemsTwo);
            exit();


            if (array_key_exists($modifiersEncodedItem,$modifiersSelectedItemsTwo)) {

                $modifierPrice=0;
                foreach ($cartItems['ModifiersItemCheckbox'] as $key=>$items){
                    if (isset($items['modifiers_id'])){
                        $modifierPrice+=$items['modifiers_price'];
                    }
                }

                $getSessionQty                         =$cartItems['quantity'];
                $newQty                                =$getSessionQty+$quantity;
                $data['quantity']                      = $newQty;
                $data['totalSalesPrice']               = ($newQty*$OriginalPrice)+$modifierPrice;
                $_SESSION['cart'][$sessionIndex]       =$data; //Updated Quantity In Same Size

            }else{
                if (array_push($_SESSION['cart'], $data)) {
                    $output['message'] = 'Item added to cart';
                }
            }
        }else {
            if (array_push($_SESSION['cart'], $data)) {
                $output['message'] = 'Item added to cart';
            } else {
                $output['error'] = true;
                $output['message'] = 'Cannot add item to cart';
            }
        }

		if ($type==1){
		    $Url=$baseUrl.'shop.php';
            header('Location: '.$Url.'');
            exit();
        }
	}

	print_r(json_encode($output));
	exit();

?>