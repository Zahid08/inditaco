<?php

require('connection.inc.php');
session_start();

if (isset($_POST)){
    $productId       =$_POST['productId'];
    $variationsId    =$_POST['variationsId'];
    $cartId         =$_POST['cart_id'];

    if(isset($_SESSION['user'])){
        $userId             =$_SESSION['id'];

        $sql                = "SELECT * FROM cart_item where id=$cartId";
        $result             = mysqli_query($conn, $sql);
        $rowsCart          = mysqli_fetch_row($result);

        $cartID=$rowsCart[0];

        $sqlSubItems = "DELETE FROM cart_modifier WHERE cart_item_id=$cartID";
        mysqli_query($conn, $sqlSubItems);

        //Delete Paretn Items
        $sqlParent = "DELETE FROM cart_item WHERE id=$cartId";
        if (mysqli_query($conn, $sqlParent)) {
           echo 1;
           exit();
        } else {
            echo "Error deleting record: " . mysqli_error($conn);
        }
    }else{
        if(!isset($_SESSION['cart'])){
            $_SESSION['cart'] = array();
        }

        $exist=array();

        $reloadArray=[];
        foreach($_SESSION['cart'] as $row){
            array_push($exist, $cartId);
            $reloadArray[$row['cart_id']]=$row;
        }

        if(in_array($cartId, $exist)){
            unset($reloadArray[$cartId]);
        }

        $_SESSION['cart']=$reloadArray;

        echo 1;
        exit();
    }
}