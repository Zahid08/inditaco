<?php
    require('connection.inc.php');
    require('cart_details.php');

    session_start();

    if(isset($_SESSION['user'])){

        $cartItemsList = getCartItems($conn, $bid);
        $paymentOptions=$_POST['PaymentOptions'];
        $addressId=$_POST['AddressId'];
        $randomNumber =str_pad(rand(0, pow(10, 7)-1), 7, '0', STR_PAD_LEFT);
        $generatedInvoiceId='INV-'.$randomNumber;
        $userId             =$_SESSION['id'];

        $notes=isset($_POST['notes'])?$_POST['notes']:'';

        $suntotal = 0;
        if ($cartItemsList) {
            foreach ($cartItemsList as $keyItemIndex => $items) {
                $suntotal += $items['totalSalesPrice'];
            }
        }

        $sqlInsert = "INSERT INTO order_table (bid,uid,invoice_id, sub_total ,total,order_status ,cart_note ,currency,payment_method) VALUES ($bid,$userId,'".$generatedInvoiceId."',$suntotal,$suntotal,'active','".$notes."','$','".$paymentOptions."')";
        if (mysqli_query($conn, $sqlInsert)) {
            $last_id = $conn->insert_id;
            foreach ($cartItemsList as $keyItemIndex => $items) {
                $cartId = $keyItemIndex;

                $productId=$items['productid'];
                $variationsId=$items['variationsId'];
                $totalSalesPrice=$items['totalSalesPrice'];
                $qty            =$items['quantity'];

                $modifierIdListOne=[];
                $modifiersEncodedItem='';

                if (isset($items['ModifiersItemCheckbox'])){
                    foreach ($items['ModifiersItemCheckbox'] as $key=>$items){
                        if (isset($items->modifiers_id)){
                            array_push($modifierIdListOne, $items->modifiers_id);
                        }
                    }
                    $modifiersEncodedItem=serialize($modifierIdListOne);
                }

                $sqlInsert1 = "INSERT INTO order_items (order_id,product_id,size,price,qty,item_modifiers) VALUES ($last_id,$productId,$variationsId,$totalSalesPrice,$qty,'".$modifiersEncodedItem."')";
                mysqli_query($conn, $sqlInsert1);

                $sqlSubItems = "DELETE FROM cart_modifier WHERE cart_item_id=$cartId";
                mysqli_query($conn, $sqlSubItems);

                //Delete Paretn Items
                $sqlParent = "DELETE FROM cart_item WHERE id=$cartId";
                mysqli_query($conn, $sqlParent);
            }
        }

        $Url=$baseUrl.'success.php?Invoice='.$generatedInvoiceId;
        header('Location: '.$Url.'');
        exit();

    }else{
        $Url=$baseUrl.'auth.php';
        header('Location: '.$Url.'');
        exit();
    }
?>