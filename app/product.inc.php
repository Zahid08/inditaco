<?php

require('connection.inc.php');

function getAllCategory($conn,$bid){
    $categoryList=[];

    $sqlCategory      = "SELECT * FROM product_category where bid=$bid";
    $resultCategory   = mysqli_query($conn, $sqlCategory);

    if ($resultCategory->num_rows > 0) {
        return $resultCategory;
    } else {
        echo "0 results";
    }
}

function getProducts( $categoryId ='', $conn=null  ,$bid=null ){

    if ($categoryId=='') {
        $sql = "SELECT * FROM product where visibility=1 and bid=$bid";
    }else{
        $sql = "SELECT * FROM product where visibility=1 and bid=$bid and category_id=$categoryId";
    }

    $result     = mysqli_query($conn, $sql);

    $productList=[];

    if ($result->num_rows > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $productId          =$row['id'];
            $getVariations      =getVariationsProduct($conn,$productId ,$bid);
            $modifiersSetId     =getProductModifiers($conn,$productId ,$bid);

            $productList []=[
                'product_id'        =>$productId,
                'product_name'      =>$row['product_name'],
                'product_desc'      =>$row['product_desc'],
                'product_slug'      =>$row['product_slug'],
                'category_id'       =>$row['category_id'],
                'featured'          =>$row['featured'],
                'best_seller'       =>$row['best_seller'],
                'modifiers_set'     =>$modifiersSetId,
                'variations'        =>$getVariations,
            ];
        }

        return $productList;

    } else {
        echo "0 results";
    }
}

function getVariationsProduct($conn,$productId=null,$bid=null){
    $variations=[];
    if ($productId=='') {
        return $variations;
    }

    $sqlVariations      = "SELECT * FROM product_size where bid=$bid and pid=$productId";
    $resultVariations   = mysqli_query($conn, $sqlVariations);

    $productList=[];

    if ($resultVariations->num_rows > 0) {
        while($rowVariations = mysqli_fetch_assoc($resultVariations)) {
            $discount=0;
            if ($rowVariations['sale_price']>$rowVariations['price']){
                $discount=(($rowVariations['sale_price']-$rowVariations['price'])/$rowVariations['sale_price'])*100;
                $discount=number_format($discount, 2, '.', '');
            }
            $variations [$rowVariations['is_default']]=[
                'v_id'          =>$rowVariations['id'],
                'v_name'        =>$rowVariations['name'],
                'v_price'       =>$rowVariations['price'],
                'v_max_qty'     =>$rowVariations['max_qty'],
                'v_sale_price'  =>$rowVariations['sale_price'],
                'v_discount'    =>$discount,
                'v_sku'         =>$rowVariations['sku'],
                'v_image'       =>$rowVariations['image'],
                'v_is_default'  =>$rowVariations['is_default'],
            ];
        }
    }

    return $variations;
}

function getProductModifiers($conn,$productId=null,$bid=null){

    if ($productId=='') {
        return '';
    }

    $productModifiersSql      = "SELECT * FROM product_modifier where pid=$productId";
    $resultProductModifier    = mysqli_query($conn, $productModifiersSql);

    $returnList=[];
    if ($resultProductModifier->num_rows > 0) {
        while($rowsItem = mysqli_fetch_assoc($resultProductModifier)) {
            $returnList['modifier_set_id']  =$rowsItem['modifier_set_id'];

            $setId=$rowsItem['modifier_set_id'];
            $sqlmodifierSet      = "SELECT * FROM modifier_set where id=$setId";
            $resultModifierSet   = mysqli_query($conn, $sqlmodifierSet);

            $returnList['checkbox']  ='';
            if ($resultModifierSet->num_rows>0){
                $itemRow = mysqli_fetch_row($resultModifierSet);
                $returnList['checkbox']=$itemRow[5];
            }

            break;
        }
    }

    return $returnList;
}

function getMainModifersList($conn ,$bid ,$productId){
    if ($productId=='') {
        return '';
    }

    $productModifiersSql      = "SELECT * FROM product_modifier where pid=$productId";
    $resultProductModifier    = mysqli_query($conn, $productModifiersSql);

    $returnList=[];

    if ($resultProductModifier->num_rows > 0) {
        while($rowsItem = mysqli_fetch_assoc($resultProductModifier)) {

            $setId                  =$rowsItem['modifier_set_id'];
            $sqlmodifierSet      = "SELECT * FROM modifier_set where id=$setId";
            $resultModifierSet   = mysqli_query($conn, $sqlmodifierSet);
            $rowsItemSet         = mysqli_fetch_row($resultModifierSet);

            if ($rowsItemSet){

                $modifierSetId              =$rowsItemSet['0'];
                $sqlmodifierSetItems      = "SELECT * FROM modifier where modifier_set_id=$modifierSetId";
                $resultModifierSetItems   = mysqli_query($conn, $sqlmodifierSetItems);

                $modifiersItem=[];
                while($rowsItemSetItems = mysqli_fetch_assoc($resultModifierSetItems)) {
                    $modifiersItem[]=$rowsItemSetItems;
                }

                $returnList[$rowsItem['id']]=[ //Product Modifier Id
                    'modifier_set_id'   =>$modifierSetId,
                    'modifier_set_name'=>$rowsItemSet[1],
                    'checkbox'          =>$rowsItemSet[5],
                    'optionsList'       =>$modifiersItem,
                ];

            }
        }

        return $returnList;
    }


    $sqlModifers            = "SELECT * FROM modifier where modifier_set_id=$productId";
    $resultModifiers        = mysqli_query($conn, $sqlModifers);

    if ($resultModifiers->num_rows > 0) {
        return $resultModifiers;
    }else{
        return "";
    }
}

/*
 * Note: Check Business Status Are open or Not
 * @Param : Db Connection, business Id
 * @Return : Void
 * */
function getBusiness($conn=null  ,$bid=null ){
    $returnOpeningStatus=0;
    $sqlForBusiness      = "SELECT * FROM business where bid=$bid and is_open='open'";
    $resultBusiness      = mysqli_query($conn, $sqlForBusiness);

    if ($resultBusiness->num_rows > 0) {
        $returnOpeningStatus=1;
    }

    return $returnOpeningStatus;
}

//
function getModifiersDiet($conn=null,$modifierId=null){
    $colorCode='';
    $sqlForModifierDiet     = "SELECT * FROM modifier_diet where modifier_id=$modifierId";
    $resultModifierDiet      = mysqli_query($conn, $sqlForModifierDiet);
    if ($resultModifierDiet->num_rows > 0) {

        $rowsItemSet         = mysqli_fetch_row($resultModifierDiet);
        if ($rowsItemSet[2]=='vegetarian'){
            $colorCode='green';
        }else{
            $colorCode='red';
        }
    }
    return $colorCode;
}