<?php

require('connection.inc.php');

if (isset($_POST)){
    $variationsId       =$_POST['varitionId'];

    $sqlVariations      = "SELECT * FROM product_size where bid=$bid and id=$variationsId";
    $resultVariations   = mysqli_query($conn, $sqlVariations);

    if ($resultVariations->num_rows > 0) {
        $singleRow = mysqli_fetch_row($resultVariations);
        $salePrice      =$singleRow[4];
        $originalPrice  =$singleRow[3];

        $discount=0;

        if ($salePrice>$originalPrice){
            $discount=(($salePrice-$originalPrice)/$salePrice)*100;
            $discount=number_format($discount, 2, '.', '');
        }

        $dataList=[
            'sales_price'      =>$singleRow[4], //Sales Price
            'max_qty'          =>$singleRow[6], //Maximum Quantity
            'discount'        =>$discount,  //Discount
            'originalPrice'   =>$originalPrice,  //Discount
        ];
        print_r(json_encode($dataList)); //Sales Price Return
        exit();

    }else{
        echo 0;
        exit();
    }
}