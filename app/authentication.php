<?php
$data = stripslashes(file_get_contents("php://input"));
$mydata = json_decode($data, true);


if(isset($mydata['submit']) && $mydata['type'] == 1){		

    require('connection.inc.php');
    require('function.inc.php');

    $full_name=get_safe_value($conn,$mydata['full_name']);
	$email=get_safe_value($conn,$mydata['email']);
    $password=get_safe_value($conn,$mydata['password']);
	$retype_password=get_safe_value($conn,$mydata['retype_password']);
    $date_of_birth= $mydata['date_of_birth'];
	$gender=get_safe_value($conn,$mydata['gender']);
    

    if($password !== $retype_password){

        echo json_encode(array("statusCode"=>201));
        exit();
    }    

    if(invalidEmail($email) !== false){

        echo json_encode(array("statusCode"=>202));
        exit();

    }

    if(emailExists($conn, $email, $bid) !== false) {

        echo json_encode(array("statusCode"=>203));
        exit();
    }

   
    registerUser($conn, $full_name, $email, $password, $gender, $date_of_birth, $bid);     
    mysqli_close($conn);
	
	
}

if(isset($mydata['submit']) && $mydata['type'] == 2){		

    require('connection.inc.php');
    require('function.inc.php');

    
	$email=get_safe_value($conn,$mydata['email']);
    $password=get_safe_value($conn,$mydata['password']);	
    
    
    loginUser($conn, $email, $password, $bid);     

    mysqli_close($conn);
	
	
}



else{
    echo json_encode(array("statusCode"=>200));
    exit();
}
