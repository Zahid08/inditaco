<?php

require('connection.inc.php');


function pr($arr){
	echo '<pre>';
	print_r($arr);
}

function prx($arr){
	echo '<pre>';
	print_r($arr);
	die();
}

function get_safe_value($conn,$str){
	if($str!=''){
		$str=trim($str);
		return mysqli_real_escape_string($conn,$str);
	}
}

function invalidEmail($email){

	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$result = true;
	} else {
		$result = false;
	}
	return $result;
}

function emailExists($conn, $email, $bid){	

    $sql = "SELECT * FROM users WHERE email = ? AND bid = ?";
	
    $stmt = mysqli_stmt_init($conn);
    if(!mysqli_stmt_prepare($stmt, $sql)){
        echo json_encode(array("statusCode"=>205));
        exit();
    }

    mysqli_stmt_bind_param($stmt, "si" , $email, $bid);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if($row = mysqli_fetch_assoc($resultData)){
        return $row;
    }
    else{
        $result = false;
        return $result;
    }

    mysqli_stmt_close($stmt);
}

function registerUser($conn, $full_name, $email, $password, $gender, $date_of_birth, $bid){
	
	
	$sql ="INSERT INTO users (full_name, email, password, gender, bid, date_of_birth) VALUES (?,?,?,?,?,?)";
	
	$stmt = mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt, $sql)){
        echo json_encode(array("statusCode"=>205));
        exit();
    }

	$hashed_password = password_hash($password, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "ssssis" , $full_name, $email, $hashed_password, $gender, $bid, $date_of_birth);
    mysqli_stmt_execute($stmt);
	echo json_encode(array("statusCode"=>207));
	exit();
}

function loginUser($conn, $email, $password, $bid){
    $emailExists= emailExists($conn, $email, $bid);

    if($emailExists === false){
        echo json_encode(array("statusCode"=>201));
	    exit();
    }

    $pwdHashed = $emailExists["password"];
    $checkedpassword = password_verify($password, $pwdHashed);

    if($checkedpassword === false){
        echo json_encode(array("statusCode"=>202));
	    exit();
    } else if ($checkedpassword === true){

        session_start();

        $_SESSION["id"] = $emailExists["id"];
        $_SESSION["name"] = $emailExists["full_name"];
        $_SESSION["user"] = "yes";
        
        echo json_encode(array("statusCode"=>206));
	    exit();

    }

}