<?php

	function getCartItems($conn,$bid)
	{
		$output = '';
		if (isset($_SESSION['user'])) {

			$userId=$_SESSION['id'];

			//If session Then Store Into Database
			if (isset($_SESSION['cart'])) {
				foreach ($_SESSION['cart'] as $row) {

					$productId			 	=$row['productid'];
					$variationsId			 =$row['variationsId'];
					$totalSalesPrice	    =$row['totalSalesPrice'];
					$generalPrice		    =$row['OriginalPrice'];
					$quantity	   			 =$row['quantity'];
					$generalPrice		    =$row['OriginalPrice'];
					$note	   			    =!empty($row['Note'])?$row['Note']:'';
					$content                 =json_encode($row);


					$modifierIdArray=[];
					$modifierItems='';
					if ($row['ModifierSet']){
						foreach ($row['ModifiersItemCheckbox'] as $key=>$itemsModifiers){
							if (isset($itemsModifiers['modifiers_id'])){
								array_push($modifierIdArray, $itemsModifiers['modifiers_id']);
							}
						}
						$modifierItems=serialize($modifierIdArray);
					}

					$cartItemsSql     	 = "SELECT * FROM cart_item where bid=$bid and  uid=$userId and product_id=$productId and product_size_id=$variationsId";
					$resultItemsCart     = mysqli_query($conn, $cartItemsSql);

					$type='';
					if (isset($row['ModifiersItemCheckbox'])){
						$type=1;
					}

					if ($resultItemsCart->num_rows > 0 && $type!=1) {
						$data         = mysqli_fetch_row($resultItemsCart);
						$ID=$data[0];
						$sql = "UPDATE cart_item SET uid=$userId,product_id=$productId,product_size_id=$variationsId,total_price=$totalSalesPrice,qty=$quantity WHERE id=$ID";
						mysqli_query($conn, $sql);
					}else{
						$sqlInsert = "INSERT INTO cart_item (bid,uid,product_id, product_size_id ,price,total_price ,qty ,content,note,selected_modifiers) VALUES ($bid,$userId,$productId,$variationsId,$generalPrice,$totalSalesPrice,$quantity,'".$content."','".$note."','".$modifierItems."')";
						if (mysqli_query($conn, $sqlInsert)) {
							$last_id = $conn->insert_id;
							if (isset($row['ModifiersItemCheckbox'])) {
								$modifiersList = $row['ModifiersItemCheckbox'];
								foreach ($modifiersList as $key => $items) {
									if (isset($items['modifiers_id'])) {
										$modifierId = $items['modifiers_id'];
										$price = $items['modifiers_price'];
										$sqlCartModifiers = "INSERT INTO cart_modifier (cart_item_id, modifier_id, price ) VALUES ($last_id,$modifierId,$price)";
										mysqli_query($conn, $sqlCartModifiers);
									}
								}
							}
						}
					}
				}
				unset($_SESSION['cart']);
			}

			$getCartItems       = "SELECT * FROM cart_item where uid=$userId";
			$resultCartIems   	= mysqli_query($conn, $getCartItems);

			$dataList=[];
			while($rowsItem = mysqli_fetch_assoc($resultCartIems)) {
				$dataList [$rowsItem['id']]=(array)json_decode($rowsItem['content']);
			}

			return $dataList;

		} else {
			if(isset($_SESSION['cart'])) {
				if (count($_SESSION['cart']) != 0) {
					return $_SESSION['cart'];
				} else {
					return [];
				}
			}

		}
	}

	function getSingleProduct($productId,$variationsId,$bid,$conn){
		$productItemsList =[];
		if ($productId=='') {
			return $productItemsList;
		}

		$sqlProduct      = "SELECT * FROM product where bid=$bid and id=$productId";
		$resultProduct   = mysqli_query($conn, $sqlProduct);

		if ($resultProduct->num_rows > 0) {
			$itemRowProduct = mysqli_fetch_row($resultProduct);
			$productItemsList['product_name']	=$itemRowProduct[1];
			$productItemsList['product_id']		=$itemRowProduct[0];
		}

		$sqlVariations      = "SELECT * FROM product_size where bid=$bid and id=$variationsId";
		$resultVariations   = mysqli_query($conn, $sqlVariations);

		if ($resultVariations->num_rows > 0) {
			$itemRow = mysqli_fetch_row($resultVariations);

			$productItemsList['variationsId']		=$variationsId;
			$productItemsList['v_name']				=$itemRow[1];
			$productItemsList['v_sale_price']		=$itemRow[4];

		}

		return $productItemsList;
	}

	function getShippingAddress($conn,$bid)
	{
		$userId = $_SESSION['id'];

		$sqlAddress = "SELECT * FROM delivery_address where bid=$bid and uid=$userId";
		$resultAddress = mysqli_query($conn, $sqlAddress);

		$addressInfo=[];
		while($rowsItem = mysqli_fetch_assoc($resultAddress)) {
			$address=$rowsItem['first_line'].','.$rowsItem['second_line'].','.$rowsItem['city'].','.$rowsItem['state'].','.$rowsItem['pincode'].','.$rowsItem['landmark'].','.$rowsItem['country'];
			$addressInfo[$rowsItem['id']]=[
				'address_id'=>$rowsItem['id'],
				'area'=>$rowsItem['area'],
				'address'=>$address,
			];
		}

		return $addressInfo;
	}
?>

