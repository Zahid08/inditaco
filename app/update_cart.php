<?php
    require('connection.inc.php');
    session_start();

    if(isset($_SESSION['user'])){
        if ($_POST['CartDetails']){
            foreach ($_POST['CartDetails'] as $keyCartId=>$valuesItem){

                $newQuantity    =$valuesItem['quantity'];
                $maxQuantity    =$valuesItem['maxqty'];

                $getTableCartItemSql     	    = "SELECT * FROM cart_item where id=$keyCartId";
                $resultItemsCartTableResult     = mysqli_query($conn, $getTableCartItemSql);

                if ($resultItemsCartTableResult->num_rows > 0) {

                    $dataSqlItem         = mysqli_fetch_row($resultItemsCartTableResult);
                    $cartItemId          =$dataSqlItem[0];
                    $previousCarditem    =json_decode($dataSqlItem[13]);
                    $OriginalPrice       =$dataSqlItem[6];

                    $modifierPrice=0;

                    if ($dataSqlItem[12]) {
                        foreach ($previousCarditem->ModifiersItemCheckbox as $key => $items) {
                            if (isset($items->modifiers_id)) {
                                $modifierPrice += $items->modifiers_price;
                            }
                        }

                        $data['ModifiersItemCheckbox']          = $previousCarditem->ModifiersItemCheckbox;
                    }



                    $newQty              =$newQuantity;
                    $totalSalesPrice     = ($newQty*$OriginalPrice)+$modifierPrice;

                    $data['productid']                      = $previousCarditem->productid;
                    $data['OriginalPrice']                  = $previousCarditem->OriginalPrice;
                    $data['quantity']                       = $newQty;
                    $data['variationsId']                   = $previousCarditem->variationsId;
                    $data['totalSalesPrice']                = $totalSalesPrice;
                    $data['ModifierSet']                    = $previousCarditem->ModifierSet;
                    $data['Note']                           = $previousCarditem->Note;
                    $data['maxQuantity']                    = $maxQuantity;

                    $content                                =json_encode($data);

                    $sql = "UPDATE cart_item SET qty=$newQty,total_price=$totalSalesPrice,content='".$content."'  WHERE id=$cartItemId";
                    mysqli_query($conn, $sql);

                }

            }

            $Url=$baseUrl.'cart_page.php';
            header('Location: '.$Url.'');
        }
    }else{
        if(!isset($_SESSION['cart'])){
            $_SESSION['cart'] = array();
        }

        foreach ($_POST['CartDetails'] as $keyCartId=>$valuesItem){

            $index          =$keyCartId;
            $newQuantity    =$valuesItem['quantity'];
            $maxQuantity         =$valuesItem['maxqty'];

            $cartItems                             =$_SESSION['cart'][$index] ;
            $OriginalPrice                         =$cartItems['OriginalPrice'];

            $modifierPrice=0;
            if (isset($cartItems['ModifiersItemCheckbox'])){
                foreach ($cartItems['ModifiersItemCheckbox'] as $key => $items) {
                    if (isset($items['modifiers_id'])) {
                        $modifierPrice += $items['modifiers_price'];
                    }
                }
                $data['ModifiersItemCheckbox']     = $cartItems['ModifiersItemCheckbox'];
            }

            $newQty              =$newQuantity;
            $totalSalesPrice     = ($newQty*$OriginalPrice)+$modifierPrice;


            $data['cart_id']                       = $cartItems['cart_id'];
            $data['productid']                      = $cartItems['productid'];
            $data['OriginalPrice']                  = $cartItems['OriginalPrice'];
            $data['quantity']                       = $newQuantity;
            $data['variationsId']                   = $cartItems['variationsId'];
            $data['totalSalesPrice']                = $totalSalesPrice;
            $data['ModifierSet']                    = $cartItems['ModifierSet'];
            $data['Note']                           = $cartItems['Note'];
            $data['maxQuantity']                    = $maxQuantity;

            $_SESSION['cart'][$index]           =$data;

        }
        $Url=$baseUrl.'cart_page.php';
        header('Location: '.$Url.'');

    }

?>