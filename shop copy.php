<?php
include "includes/header.php";
require('../inditaco/app/product.inc.php');
require('../inditaco/app/connection.inc.php');

$categoryId     = isset($_REQUEST['category_id']) ? $_REQUEST['category_id'] : '';
$productList     = getProducts($categoryId, $conn, $bid);
$categoryList    = getAllCategory($conn, $bid);
?>
<style>
    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
    }

    .overlay:target {
        visibility: visible;
        opacity: 1;
        z-index: 1000;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }

    .popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }

    .popup .close:hover {
        color: #06D85F;
    }

    .popup .content {
        max-height: 30%;
        overflow: auto;
        margin-top: 20px;
    }

    @media screen and (max-width: 700px) {
        .box {
            width: 70%;
        }

        .popup {
            width: 70%;
        }
    }
</style>

<section class="body-font" style="background-image: url(assets/images/slider-image-1.jpg); height:380px">
    <div class="container mx-auto flex px-8 py-24 md:flex-row flex-col items-center">
        <div class="lg:flex-grow md:w-1/2 mt-20 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font mb-4 mt-3 text-6xl text-white">Order Now</h1>
            <p class="mb-6 leading-relaxed subtitle-font text-xl text-white ">Lorem Ipsum some tagline about us or our story</p>
        </div>
    </div>
</section>
<div class="bg-texture">
    <section class=" body-font">
        <div class="container px-8 py-16 mb-16 mx-auto">
            <div class="flex flex-wrap -m-4">
                <div class="p-4 lg:w-3/12">
                    <div class="h-full px-8 pt-16 pb-24 rounded-lg overflow-hidden relative">
                        <p class="mb-6 leading-relaxed subtitle-font text-xl sc-color">Our Product Range</p>
                        <h1 class="title-font mb-4 mt-3 text-4xl text-white">Best Taco From <br class="hidden lg:inline-block"> India</h1>
                        <div class="h-full pt-16 pb-24 rounded-lg text-white overflow-hidden relative">
                            <ul class="item-menu">
                                <li class="active"><a href="<?= $baseUrl ?>shop.php">All Items</a></li>
                                <?php while ($rowCategory = mysqli_fetch_assoc($categoryList)) { ?>
                                    <li><a href="<?= $baseUrl ?>shop.php?category_id=<?= $rowCategory['cat_id'] ?>"><?= $rowCategory['category_name'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="p-4 lg:w-9/12 ">
                    <div class="callout" id="callout" style="margin-left: 22px;display: none">
                        <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
                        <span class="message"></span>
                    </div>

                    <section class=" body-font overflow-hidden">
                        <div class="container px-8 pt-12  pb-4 mx-auto">
                            <div class="flex flex-wrap -m-6">
                                <?php
                                if ($productList) {
                                    foreach ($productList as $keyItemIndex => $items) {
                                ?>
                                        <div class="px-6 py-6 md:w-1/3 flex flex-col items-start" id="Product-cart-items">
                                            <img src="<?= $items['variations']['yes']['v_image'] ?>">
                                            <p class="my-2 leading-relaxed-sub  subtitle-font text-xl sc-color"></p>
                                            <h2 class="sm:text-2xl text-lg title-font text-white h-16 mb-4"><?= $items['product_name'] ?></h2>
                                            <p class="leading-relaxed subtitle-font text-lg text-white "><?= $items['product_desc'] ?></p>
                                            <div class="sm:text-base text-base title-font flex text-white my-4">
                                                <p>$<span id="salesPrice-<?= $keyItemIndex ?>"><?= $items['variations']['yes']['v_sale_price'] ?></span></p>
                                                <input type="hidden" id="hiddenDefaultPrice-<?= $keyItemIndex ?>" value="<?= $items['variations']['yes']['v_sale_price'] ?>">
                                            </div>
                                            <div style="display: inherit;">
                                                <div class="mb-3 w-3/3">
                                                    <div class="relative flex">
                                                        <select class="combine variations-<?= $keyItemIndex ?>" id="variations" name="variations" data-index="<?= $keyItemIndex ?>" data-product-id="<?= $items['product_id'] ?>">
                                                            <?php
                                                            foreach ($items['variations'] as $key => $itemsVariations) {
                                                            ?>
                                                                <option value="<?= $itemsVariations['v_id'] ?>"><?= $itemsVariations['v_name'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="qtydiv">
                                                    <div class="qtybox">
                                                        <span class="btnqty qtyminus icon icon-minus" data-index="<?= $keyItemIndex ?>">-</span>
                                                        <input type="text" id="quantity" name="quantity" value="1" min="1" class="quantity-selector quantity-input select-quantity-<?= $keyItemIndex ?>" readonly="">
                                                        <span class="btnqty qtyplus icon icon-plus" data-index="<?= $keyItemIndex ?>">+</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php if (isset($items['modifiers_set']['modifier_set_id'])) { ?>
                                                <?php
                                                $getModifersList = getMainModifersList($conn, $bid, $items['product_id']);
                                                ?>
                                                <a class="cart-button" href="#popup<?= $keyItemIndex ?>" style="width: 83%;text-align: center;" id="add-to-cart-modal" data-index="<?= $keyItemIndex ?>" data-product-id="<?= $items['product_id'] ?>">Add to Cart</a>
                                                <div id="popup<?= $keyItemIndex ?>" class="overlay">
                                                    <form method="post" action="<?= $baseUrl ?>app/cart_add.php?type=1">
                                                        <input type="hidden" name="productid" value="<?= $items['product_id'] ?>">
                                                        <div class="popup">
                                                            <div class="flex justify-between mb-8">
                                                                <h1><?= $items['product_name'] ?></h1>
                                                                    <a class="close" style="padding: 6px; background-color: #e0e0e0; border-radius:4px" href="#"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                                                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                                                    </svg>
                                                                    </a>
                                                            </div>
                                                           
                                                        </div>
                                                    </form>
                                                </div>
                                            <?php } else { ?>
                                                <button class="cart-button add-to-cart-<?= $keyItemIndex ?>" id="add-to-cart" type="submit" style="width: 83%;" data-index="<?= $keyItemIndex ?>" data-product-id="<?= $items['product_id'] ?>">Add to Cart</button>
                                            <?php } ?>
                                        </div>
                                    <?php }
                                } else { ?>
                                    <h1 class="title-font mb-4 mt-3 text-4xl text-white">No Data Available On This Category</h1>
                                <?php } ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

</div>





<script>
    $(document).off('click', 'span.btnqty.qtyplus.icon.icon-plus,span.btnqty.qtyminus.icon.icon-minus').on('click', 'span.btnqty.qtyplus.icon.icon-plus,span.btnqty.qtyminus.icon.icon-minus', function() {
        var currentIndx = $(this).data('index');
        var salesPrice = $('input#hiddenDefaultPrice-' + currentIndx + '').val();
        var quantity = $('input.select-quantity-' + currentIndx + '').val();
        var totalSalesPrice = parseInt(salesPrice * quantity);
        totalSalesPrice = (isNaN(totalSalesPrice)) ? 1 : totalSalesPrice;
        $('span#salesPrice-' + currentIndx + '').html(totalSalesPrice);
    });


    var arrayModifierId = [];

    $(document).ready(function() {
        $("input#modifierName").change(function() {

            var currentIndex = $(this).data('index');
            var InputTypeCheck = $(this).attr('type');

            var InputType = 0;
            if (InputTypeCheck == 'checkbox') {
                InputType = 1;
            }

            if (InputType == 0) { //Uncehck From backend

                var PreviousSelectedData = parseFloat($('input#radioSelectedItem-' + currentIndex + '').val());
                var addOrderPrice = parseFloat($('span#modaladdOrderPayment-' + currentIndex + '').html());
                var newPrice = addOrderPrice - PreviousSelectedData;
                var totalAmount = parseFloat($(this).data('modifiere-price') + parseFloat(newPrice));

                if ($("input#modifierName").is(":checked")) {
                    $('span#modaladdOrderPayment-' + currentIndex + '').html(totalAmount);

                    $('input#TotalSalesPrice-' + currentIndex + '').val(totalAmount);

                    arrayModifierId = [];
                    arrayModifierId.push($(this).val());

                    $('input#selectedModifiers-' + currentIndex + '').val(JSON.stringify(arrayModifierId));

                    $('input#radioSelectedItem-' + currentIndex + '').val($(this).data('modifiere-price'));
                }
            } else {
                if ($(this).is(":checked")) {
                    var totalAmount = parseFloat($(this).data('modifiere-price') + parseFloat($('span#modaladdOrderPayment-' + currentIndex + '').html()));
                    $('span#modaladdOrderPayment-' + currentIndex + '').html(totalAmount);
                    $('input#TotalSalesPrice-' + currentIndex + '').val(totalAmount);
                } else {
                    var subtractAmount = parseFloat($('span#modaladdOrderPayment-' + currentIndex + '').html() - parseFloat($(this).data('modifiere-price')));
                    $('span#modaladdOrderPayment-' + currentIndex + '').html(subtractAmount);
                    $('input#TotalSalesPrice-' + currentIndex + '').val(totalAmount);
                }
            }
        });
    });


    $(document).off('click', 'a#add-to-cart-modal').on('click', ' a#add-to-cart-modal', function() {
        var currentIndx = $(this).data('index');
        var salesPrice = $('input#hiddenDefaultPrice-' + currentIndx + '').val();
        var quantity = $('input.select-quantity-' + currentIndx + '').val(); //Quantity
        var totalSalesPrice = parseInt(salesPrice * quantity);
        totalSalesPrice = (isNaN(totalSalesPrice)) ? 1 : totalSalesPrice; //Sales Price
        var variationsId = $('select.variations-' + currentIndx + '').val();

        $('input#VariationsId-' + currentIndx + '').val(variationsId);
        $('input#Quantity-' + currentIndx + '').val(quantity);
        $('input#TotalSalesPrice-' + currentIndx + '').val(totalSalesPrice);
        $('input#previousSelectedPrice-' + currentIndx + '').val(totalSalesPrice);
        $('span#modaladdOrderPayment-' + currentIndx + '').html(totalSalesPrice);
    });


    $(document).off('click', 'button#add-to-cart').on('click', ' button#add-to-cart', function() {
        var currentIndx = $(this).data('index');
        var variationsId = $('select.variations-' + currentIndx + '').val();
        var salesPrice = $('input#hiddenDefaultPrice-' + currentIndx + '').val();
        var quantity = $('input.select-quantity-' + currentIndx + '').val(); //Quantity
        var totalSalesPrice = parseInt(salesPrice * quantity);
        totalSalesPrice = (isNaN(totalSalesPrice)) ? 1 : totalSalesPrice; //Sales Price
        var productid = $(this).data('product-id'); //Product Id

        var RedirectUrl = '<?php echo $baseUrl; ?>shop.php';

        if (productid) {
            $.ajax({
                url: '<?php echo $baseUrl; ?>app/cart_add.php',
                data: {
                    productid: productid,
                    totalSalesPrice: totalSalesPrice,
                    quantity: quantity,
                    variationsId: variationsId,
                    currentIndx: currentIndx,
                    OriginalPrice: salesPrice,
                },
                type: "POST",
                success: function(response) {
                    var obj = jQuery.parseJSON(response);
                    $('#callout').show();
                    $('.message').html(obj.message);
                    if (obj.error) {
                        $('#callout').removeClass('callout-success').addClass('callout-danger');
                    } else {
                        $('#callout').removeClass('callout-danger').addClass('callout-success');
                    }

                    setTimeout(function() {
                        $('#callout').fadeOut('fast');
                        window.location.href = RedirectUrl;
                    }, 1000);

                },
                error: function() {
                    alert('Somethings Wrong');
                },
                beforeSend: function(xhr) {
                    $('button.add-to-cart-' + currentIndx + '').html("<span class=\"fa fa-spin fa-spinner\"></span> Adding...");
                },
            });
        }

    });

    $('.qtybox .btnqty').on('click', function() {
        var qty = parseInt($(this).parent('.qtybox').find('.quantity-input').val());
        if ($(this).hasClass('qtyplus')) {
            qty++;
        } else {
            if (qty > 1) {
                qty--;
            }
        }
        qty = (isNaN(qty)) ? 1 : qty;
        $(this).parent('.qtybox').find('.quantity-input').val(qty);
    });


    //Acitvated Deactivated
    $(document).on('change', 'select#variations', function() {
        var currentIndex = $(this).data('index');
        var productId = $(this).data('product-id');
        var varitionId = $(this).val();
        var quantity = $('input.select-quantity-' + currentIndex + '').val();

        if (productId) {
            $.ajax({
                url: '<?php echo $baseUrl; ?>app/search_product.php',
                data: {
                    productId: productId,
                    currentIndex: currentIndex,
                    varitionId: varitionId,
                },
                type: "POST",
                success: function(salesPrice) {
                    if (salesPrice) {
                        $('input#hiddenDefaultPrice-' + currentIndex + '').val(salesPrice);
                        var quantity = $('input.select-quantity-' + currentIndex + '').val();
                        var totalSalesPrice = parseInt(salesPrice * quantity);
                        totalSalesPrice = (isNaN(totalSalesPrice)) ? 1 : totalSalesPrice;
                        $('span#salesPrice-' + currentIndex + '').html(totalSalesPrice);
                    }
                },
                error: function() {
                    alert('Somethings Wrong');
                },
                beforeSend: function() {}
            });
        }
    });
</script>
<?php
include "includes/footer.php";
?>