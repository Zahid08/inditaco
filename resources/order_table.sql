-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2021 at 09:41 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salesberry`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_table`
--

CREATE TABLE `order_table` (
  `id` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `invoice_id` varchar(250) DEFAULT NULL,
  `sub_total` int(11) NOT NULL,
  `tax` int(11) NOT NULL,
  `discount_amount` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  `delivery_charges` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `order_status` enum('active','inactive') NOT NULL,
  `status_message` text NOT NULL,
  `status_date` datetime NOT NULL,
  `delivery_boy_id` int(11) NOT NULL,
  `delivery_address_id` int(11) NOT NULL,
  `order_deliver_status` enum('deliver','not_deliver','onhold','return') NOT NULL,
  `cart_note` text NOT NULL,
  `currency` int(11) NOT NULL,
  `payment_method` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_table`
--

INSERT INTO `order_table` (`id`, `bid`, `uid`, `invoice_id`, `sub_total`, `tax`, `discount_amount`, `discount_id`, `delivery_charges`, `total`, `created_on`, `order_status`, `status_message`, `status_date`, `delivery_boy_id`, `delivery_address_id`, `order_deliver_status`, `cart_note`, `currency`, `payment_method`) VALUES
(15, 21, 1, 'INV-3544510', 330, 0, 0, 0, 0, 330, '2021-09-27 19:29:47', 'active', '', '0000-00-00 00:00:00', 0, 0, 'deliver', 'xXasdasd', 0, '0'),
(16, 21, 1, 'INV-7282570', 200, 0, 0, 0, 0, 200, '2021-09-27 19:40:59', 'active', '', '0000-00-00 00:00:00', 0, 0, 'deliver', 'Aasdasd', 0, 'cash_on_delivery');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_table`
--
ALTER TABLE `order_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_table`
--
ALTER TABLE `order_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
